using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_NONVOLATILE_INTERLOCK_4
{
    public class UserModuleClass_NONVOLATILE_INTERLOCK_4 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput IN_1;
        Crestron.Logos.SplusObjects.DigitalInput IN_2;
        Crestron.Logos.SplusObjects.DigitalInput IN_3;
        Crestron.Logos.SplusObjects.DigitalInput IN_4;
        Crestron.Logos.SplusObjects.DigitalOutput OUT_1;
        Crestron.Logos.SplusObjects.DigitalOutput OUT_2;
        Crestron.Logos.SplusObjects.DigitalOutput OUT_3;
        Crestron.Logos.SplusObjects.DigitalOutput OUT_4;
        object IN_1_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 20;
                _SplusNVRAM.OUT_2A = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 21;
                _SplusNVRAM.OUT_3A = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 22;
                _SplusNVRAM.OUT_4A = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 23;
                _SplusNVRAM.OUT_1A = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 25;
                OUT_2  .Value = (ushort) ( _SplusNVRAM.OUT_2A ) ; 
                __context__.SourceCodeLine = 26;
                OUT_3  .Value = (ushort) ( _SplusNVRAM.OUT_3A ) ; 
                __context__.SourceCodeLine = 27;
                OUT_4  .Value = (ushort) ( _SplusNVRAM.OUT_4A ) ; 
                __context__.SourceCodeLine = 28;
                OUT_1  .Value = (ushort) ( _SplusNVRAM.OUT_1A ) ; 
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object IN_2_OnPush_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 33;
            _SplusNVRAM.OUT_1A = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 34;
            _SplusNVRAM.OUT_3A = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 35;
            _SplusNVRAM.OUT_4A = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 36;
            _SplusNVRAM.OUT_2A = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 38;
            OUT_2  .Value = (ushort) ( _SplusNVRAM.OUT_2A ) ; 
            __context__.SourceCodeLine = 39;
            OUT_3  .Value = (ushort) ( _SplusNVRAM.OUT_3A ) ; 
            __context__.SourceCodeLine = 40;
            OUT_4  .Value = (ushort) ( _SplusNVRAM.OUT_4A ) ; 
            __context__.SourceCodeLine = 41;
            OUT_1  .Value = (ushort) ( _SplusNVRAM.OUT_1A ) ; 
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object IN_3_OnPush_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 46;
        _SplusNVRAM.OUT_2A = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 47;
        _SplusNVRAM.OUT_1A = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 48;
        _SplusNVRAM.OUT_4A = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 49;
        _SplusNVRAM.OUT_3A = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 51;
        OUT_2  .Value = (ushort) ( _SplusNVRAM.OUT_2A ) ; 
        __context__.SourceCodeLine = 52;
        OUT_3  .Value = (ushort) ( _SplusNVRAM.OUT_3A ) ; 
        __context__.SourceCodeLine = 53;
        OUT_4  .Value = (ushort) ( _SplusNVRAM.OUT_4A ) ; 
        __context__.SourceCodeLine = 54;
        OUT_1  .Value = (ushort) ( _SplusNVRAM.OUT_1A ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IN_4_OnPush_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 59;
        _SplusNVRAM.OUT_2A = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 60;
        _SplusNVRAM.OUT_3A = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 61;
        _SplusNVRAM.OUT_1A = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 62;
        _SplusNVRAM.OUT_4A = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 64;
        OUT_2  .Value = (ushort) ( _SplusNVRAM.OUT_2A ) ; 
        __context__.SourceCodeLine = 65;
        OUT_3  .Value = (ushort) ( _SplusNVRAM.OUT_3A ) ; 
        __context__.SourceCodeLine = 66;
        OUT_4  .Value = (ushort) ( _SplusNVRAM.OUT_4A ) ; 
        __context__.SourceCodeLine = 67;
        OUT_1  .Value = (ushort) ( _SplusNVRAM.OUT_1A ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 72;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ((((_SplusNVRAM.OUT_1A + _SplusNVRAM.OUT_2A) + _SplusNVRAM.OUT_3A) + _SplusNVRAM.OUT_4A) == 0))  ) ) 
            { 
            __context__.SourceCodeLine = 74;
            _SplusNVRAM.OUT_1A = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 75;
            _SplusNVRAM.OUT_2A = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 76;
            _SplusNVRAM.OUT_3A = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 77;
            _SplusNVRAM.OUT_4A = (ushort) ( 0 ) ; 
            } 
        
        __context__.SourceCodeLine = 80;
        OUT_2  .Value = (ushort) ( _SplusNVRAM.OUT_2A ) ; 
        __context__.SourceCodeLine = 81;
        OUT_3  .Value = (ushort) ( _SplusNVRAM.OUT_3A ) ; 
        __context__.SourceCodeLine = 82;
        OUT_4  .Value = (ushort) ( _SplusNVRAM.OUT_4A ) ; 
        __context__.SourceCodeLine = 83;
        OUT_1  .Value = (ushort) ( _SplusNVRAM.OUT_1A ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    
    IN_1 = new Crestron.Logos.SplusObjects.DigitalInput( IN_1__DigitalInput__, this );
    m_DigitalInputList.Add( IN_1__DigitalInput__, IN_1 );
    
    IN_2 = new Crestron.Logos.SplusObjects.DigitalInput( IN_2__DigitalInput__, this );
    m_DigitalInputList.Add( IN_2__DigitalInput__, IN_2 );
    
    IN_3 = new Crestron.Logos.SplusObjects.DigitalInput( IN_3__DigitalInput__, this );
    m_DigitalInputList.Add( IN_3__DigitalInput__, IN_3 );
    
    IN_4 = new Crestron.Logos.SplusObjects.DigitalInput( IN_4__DigitalInput__, this );
    m_DigitalInputList.Add( IN_4__DigitalInput__, IN_4 );
    
    OUT_1 = new Crestron.Logos.SplusObjects.DigitalOutput( OUT_1__DigitalOutput__, this );
    m_DigitalOutputList.Add( OUT_1__DigitalOutput__, OUT_1 );
    
    OUT_2 = new Crestron.Logos.SplusObjects.DigitalOutput( OUT_2__DigitalOutput__, this );
    m_DigitalOutputList.Add( OUT_2__DigitalOutput__, OUT_2 );
    
    OUT_3 = new Crestron.Logos.SplusObjects.DigitalOutput( OUT_3__DigitalOutput__, this );
    m_DigitalOutputList.Add( OUT_3__DigitalOutput__, OUT_3 );
    
    OUT_4 = new Crestron.Logos.SplusObjects.DigitalOutput( OUT_4__DigitalOutput__, this );
    m_DigitalOutputList.Add( OUT_4__DigitalOutput__, OUT_4 );
    
    
    IN_1.OnDigitalPush.Add( new InputChangeHandlerWrapper( IN_1_OnPush_0, false ) );
    IN_2.OnDigitalPush.Add( new InputChangeHandlerWrapper( IN_2_OnPush_1, false ) );
    IN_3.OnDigitalPush.Add( new InputChangeHandlerWrapper( IN_3_OnPush_2, false ) );
    IN_4.OnDigitalPush.Add( new InputChangeHandlerWrapper( IN_4_OnPush_3, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_NONVOLATILE_INTERLOCK_4 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}




const uint IN_1__DigitalInput__ = 0;
const uint IN_2__DigitalInput__ = 1;
const uint IN_3__DigitalInput__ = 2;
const uint IN_4__DigitalInput__ = 3;
const uint OUT_1__DigitalOutput__ = 0;
const uint OUT_2__DigitalOutput__ = 1;
const uint OUT_3__DigitalOutput__ = 2;
const uint OUT_4__DigitalOutput__ = 3;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    [SplusStructAttribute(0, false, true)]
            public ushort OUT_1A = 0;
            [SplusStructAttribute(1, false, true)]
            public ushort OUT_2A = 0;
            [SplusStructAttribute(2, false, true)]
            public ushort OUT_3A = 0;
            [SplusStructAttribute(3, false, true)]
            public ushort OUT_4A = 0;
            
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
