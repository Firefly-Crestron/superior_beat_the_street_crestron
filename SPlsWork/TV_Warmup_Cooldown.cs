using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_TV_WARMUP_COOLDOWN
{
    public class UserModuleClass_TV_WARMUP_COOLDOWN : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput POWERON;
        Crestron.Logos.SplusObjects.DigitalInput POWEROFF;
        Crestron.Logos.SplusObjects.DigitalInput WARMINGUP;
        Crestron.Logos.SplusObjects.DigitalInput COOLINGDOWN;
        Crestron.Logos.SplusObjects.DigitalOutput ONCUE;
        Crestron.Logos.SplusObjects.DigitalOutput OFFCUE;
        ushort ONREADY = 0;
        ushort OFFREADY = 0;
        object POWERON_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 14;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( WARMINGUP  .Value ) || Functions.TestForTrue ( COOLINGDOWN  .Value )) ))  ) ) 
                    {
                    __context__.SourceCodeLine = 15;
                    ONREADY = (ushort) ( 1 ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 17;
                    Functions.Pulse ( 10, ONCUE ) ; 
                    }
                
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object POWEROFF_OnPush_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 22;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( WARMINGUP  .Value ) || Functions.TestForTrue ( COOLINGDOWN  .Value )) ))  ) ) 
                {
                __context__.SourceCodeLine = 23;
                OFFREADY = (ushort) ( 1 ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 25;
                Functions.Pulse ( 10, OFFCUE ) ; 
                }
            
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object WARMINGUP_OnRelease_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 30;
        Functions.Delay (  (int) ( 100 ) ) ; 
        __context__.SourceCodeLine = 32;
        if ( Functions.TestForTrue  ( ( ONREADY)  ) ) 
            { 
            __context__.SourceCodeLine = 34;
            Functions.Pulse ( 10, ONCUE ) ; 
            __context__.SourceCodeLine = 35;
            ONREADY = (ushort) ( 0 ) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 37;
            if ( Functions.TestForTrue  ( ( OFFREADY)  ) ) 
                { 
                __context__.SourceCodeLine = 39;
                Functions.Pulse ( 10, OFFCUE ) ; 
                __context__.SourceCodeLine = 40;
                OFFREADY = (ushort) ( 0 ) ; 
                } 
            
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 46;
        ONREADY = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 47;
        OFFREADY = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 48;
        ONCUE  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 49;
        OFFCUE  .Value = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    
    POWERON = new Crestron.Logos.SplusObjects.DigitalInput( POWERON__DigitalInput__, this );
    m_DigitalInputList.Add( POWERON__DigitalInput__, POWERON );
    
    POWEROFF = new Crestron.Logos.SplusObjects.DigitalInput( POWEROFF__DigitalInput__, this );
    m_DigitalInputList.Add( POWEROFF__DigitalInput__, POWEROFF );
    
    WARMINGUP = new Crestron.Logos.SplusObjects.DigitalInput( WARMINGUP__DigitalInput__, this );
    m_DigitalInputList.Add( WARMINGUP__DigitalInput__, WARMINGUP );
    
    COOLINGDOWN = new Crestron.Logos.SplusObjects.DigitalInput( COOLINGDOWN__DigitalInput__, this );
    m_DigitalInputList.Add( COOLINGDOWN__DigitalInput__, COOLINGDOWN );
    
    ONCUE = new Crestron.Logos.SplusObjects.DigitalOutput( ONCUE__DigitalOutput__, this );
    m_DigitalOutputList.Add( ONCUE__DigitalOutput__, ONCUE );
    
    OFFCUE = new Crestron.Logos.SplusObjects.DigitalOutput( OFFCUE__DigitalOutput__, this );
    m_DigitalOutputList.Add( OFFCUE__DigitalOutput__, OFFCUE );
    
    
    POWERON.OnDigitalPush.Add( new InputChangeHandlerWrapper( POWERON_OnPush_0, false ) );
    POWEROFF.OnDigitalPush.Add( new InputChangeHandlerWrapper( POWEROFF_OnPush_1, false ) );
    WARMINGUP.OnDigitalRelease.Add( new InputChangeHandlerWrapper( WARMINGUP_OnRelease_2, false ) );
    COOLINGDOWN.OnDigitalRelease.Add( new InputChangeHandlerWrapper( WARMINGUP_OnRelease_2, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_TV_WARMUP_COOLDOWN ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}




const uint POWERON__DigitalInput__ = 0;
const uint POWEROFF__DigitalInput__ = 1;
const uint WARMINGUP__DigitalInput__ = 2;
const uint COOLINGDOWN__DigitalInput__ = 3;
const uint ONCUE__DigitalOutput__ = 0;
const uint OFFCUE__DigitalOutput__ = 1;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
