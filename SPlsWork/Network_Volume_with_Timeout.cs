using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_NETWORK_VOLUME_WITH_TIMEOUT
{
    public class UserModuleClass_NETWORK_VOLUME_WITH_TIMEOUT : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput VOLUMEUP_PRESS;
        Crestron.Logos.SplusObjects.DigitalInput VOLUMEUP_HOLD;
        Crestron.Logos.SplusObjects.DigitalInput VOLUMEUP_RELEASE;
        Crestron.Logos.SplusObjects.DigitalInput VOLUMEDOWN_PRESS;
        Crestron.Logos.SplusObjects.DigitalInput VOLUMEDOWN_HOLD;
        Crestron.Logos.SplusObjects.DigitalInput VOLUMEDOWN_RELEASE;
        Crestron.Logos.SplusObjects.DigitalOutput VOLUP_OUT;
        Crestron.Logos.SplusObjects.DigitalOutput VOLDOWN_OUT;
        UShortParameter HOLDDWELLTIME;
        object VOLUMEUP_PRESS_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 19;
                Functions.Pulse ( 10, VOLUP_OUT ) ; 
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object VOLUMEUP_HOLD_OnPush_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 24;
            while ( Functions.TestForTrue  ( ( VOLUMEUP_HOLD  .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 26;
                Functions.Pulse ( HOLDDWELLTIME  .Value, VOLUP_OUT ) ; 
                __context__.SourceCodeLine = 24;
                } 
            
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object VOLUMEUP_RELEASE_OnPush_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 32;
        VOLUP_OUT  .Value = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMEDOWN_PRESS_OnPush_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 37;
        Functions.Pulse ( 10, VOLDOWN_OUT ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMEDOWN_HOLD_OnPush_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 42;
        while ( Functions.TestForTrue  ( ( VOLUMEDOWN_HOLD  .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 44;
            Functions.Pulse ( HOLDDWELLTIME  .Value, VOLDOWN_OUT ) ; 
            __context__.SourceCodeLine = 42;
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMEDOWN_RELEASE_OnPush_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 50;
        VOLDOWN_OUT  .Value = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}


public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    
    VOLUMEUP_PRESS = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEUP_PRESS__DigitalInput__, this );
    m_DigitalInputList.Add( VOLUMEUP_PRESS__DigitalInput__, VOLUMEUP_PRESS );
    
    VOLUMEUP_HOLD = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEUP_HOLD__DigitalInput__, this );
    m_DigitalInputList.Add( VOLUMEUP_HOLD__DigitalInput__, VOLUMEUP_HOLD );
    
    VOLUMEUP_RELEASE = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEUP_RELEASE__DigitalInput__, this );
    m_DigitalInputList.Add( VOLUMEUP_RELEASE__DigitalInput__, VOLUMEUP_RELEASE );
    
    VOLUMEDOWN_PRESS = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEDOWN_PRESS__DigitalInput__, this );
    m_DigitalInputList.Add( VOLUMEDOWN_PRESS__DigitalInput__, VOLUMEDOWN_PRESS );
    
    VOLUMEDOWN_HOLD = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEDOWN_HOLD__DigitalInput__, this );
    m_DigitalInputList.Add( VOLUMEDOWN_HOLD__DigitalInput__, VOLUMEDOWN_HOLD );
    
    VOLUMEDOWN_RELEASE = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEDOWN_RELEASE__DigitalInput__, this );
    m_DigitalInputList.Add( VOLUMEDOWN_RELEASE__DigitalInput__, VOLUMEDOWN_RELEASE );
    
    VOLUP_OUT = new Crestron.Logos.SplusObjects.DigitalOutput( VOLUP_OUT__DigitalOutput__, this );
    m_DigitalOutputList.Add( VOLUP_OUT__DigitalOutput__, VOLUP_OUT );
    
    VOLDOWN_OUT = new Crestron.Logos.SplusObjects.DigitalOutput( VOLDOWN_OUT__DigitalOutput__, this );
    m_DigitalOutputList.Add( VOLDOWN_OUT__DigitalOutput__, VOLDOWN_OUT );
    
    HOLDDWELLTIME = new UShortParameter( HOLDDWELLTIME__Parameter__, this );
    m_ParameterList.Add( HOLDDWELLTIME__Parameter__, HOLDDWELLTIME );
    
    
    VOLUMEUP_PRESS.OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEUP_PRESS_OnPush_0, false ) );
    VOLUMEUP_HOLD.OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEUP_HOLD_OnPush_1, false ) );
    VOLUMEUP_RELEASE.OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEUP_RELEASE_OnPush_2, false ) );
    VOLUMEDOWN_PRESS.OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEDOWN_PRESS_OnPush_3, false ) );
    VOLUMEDOWN_HOLD.OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEDOWN_HOLD_OnPush_4, false ) );
    VOLUMEDOWN_RELEASE.OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEDOWN_RELEASE_OnPush_5, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_NETWORK_VOLUME_WITH_TIMEOUT ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}




const uint VOLUMEUP_PRESS__DigitalInput__ = 0;
const uint VOLUMEUP_HOLD__DigitalInput__ = 1;
const uint VOLUMEUP_RELEASE__DigitalInput__ = 2;
const uint VOLUMEDOWN_PRESS__DigitalInput__ = 3;
const uint VOLUMEDOWN_HOLD__DigitalInput__ = 4;
const uint VOLUMEDOWN_RELEASE__DigitalInput__ = 5;
const uint VOLUP_OUT__DigitalOutput__ = 0;
const uint VOLDOWN_OUT__DigitalOutput__ = 1;
const uint HOLDDWELLTIME__Parameter__ = 10;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
