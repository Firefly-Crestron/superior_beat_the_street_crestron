using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_MUTE_DISCRETE_FROM_TOGGLE
{
    public class UserModuleClass_MUTE_DISCRETE_FROM_TOGGLE : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput MUTE_TOG;
        Crestron.Logos.SplusObjects.DigitalInput MUTE_IN;
        Crestron.Logos.SplusObjects.DigitalInput UNMUTE_IN;
        Crestron.Logos.SplusObjects.DigitalInput MUTE_FB;
        Crestron.Logos.SplusObjects.DigitalOutput MUTE;
        Crestron.Logos.SplusObjects.DigitalOutput UNMUTE;
        object MUTE_TOG_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 13;
                if ( Functions.TestForTrue  ( ( MUTE_FB  .Value)  ) ) 
                    {
                    __context__.SourceCodeLine = 14;
                    Functions.Pulse ( 1, UNMUTE ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 16;
                    Functions.Pulse ( 1, MUTE ) ; 
                    }
                
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object MUTE_IN_OnPush_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 21;
            Functions.Pulse ( 1, MUTE ) ; 
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object UNMUTE_IN_OnPush_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 26;
        Functions.Pulse ( 1, UNMUTE ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}


public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    
    MUTE_TOG = new Crestron.Logos.SplusObjects.DigitalInput( MUTE_TOG__DigitalInput__, this );
    m_DigitalInputList.Add( MUTE_TOG__DigitalInput__, MUTE_TOG );
    
    MUTE_IN = new Crestron.Logos.SplusObjects.DigitalInput( MUTE_IN__DigitalInput__, this );
    m_DigitalInputList.Add( MUTE_IN__DigitalInput__, MUTE_IN );
    
    UNMUTE_IN = new Crestron.Logos.SplusObjects.DigitalInput( UNMUTE_IN__DigitalInput__, this );
    m_DigitalInputList.Add( UNMUTE_IN__DigitalInput__, UNMUTE_IN );
    
    MUTE_FB = new Crestron.Logos.SplusObjects.DigitalInput( MUTE_FB__DigitalInput__, this );
    m_DigitalInputList.Add( MUTE_FB__DigitalInput__, MUTE_FB );
    
    MUTE = new Crestron.Logos.SplusObjects.DigitalOutput( MUTE__DigitalOutput__, this );
    m_DigitalOutputList.Add( MUTE__DigitalOutput__, MUTE );
    
    UNMUTE = new Crestron.Logos.SplusObjects.DigitalOutput( UNMUTE__DigitalOutput__, this );
    m_DigitalOutputList.Add( UNMUTE__DigitalOutput__, UNMUTE );
    
    
    MUTE_TOG.OnDigitalPush.Add( new InputChangeHandlerWrapper( MUTE_TOG_OnPush_0, false ) );
    MUTE_IN.OnDigitalPush.Add( new InputChangeHandlerWrapper( MUTE_IN_OnPush_1, false ) );
    UNMUTE_IN.OnDigitalPush.Add( new InputChangeHandlerWrapper( UNMUTE_IN_OnPush_2, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_MUTE_DISCRETE_FROM_TOGGLE ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}




const uint MUTE_TOG__DigitalInput__ = 0;
const uint MUTE_IN__DigitalInput__ = 1;
const uint UNMUTE_IN__DigitalInput__ = 2;
const uint MUTE_FB__DigitalInput__ = 3;
const uint MUTE__DigitalOutput__ = 0;
const uint UNMUTE__DigitalOutput__ = 1;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
