using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_2_TVS_IP_AND_MAC_SETUP
{
    public class UserModuleClass_2_TVS_IP_AND_MAC_SETUP : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput TV1SELECT;
        Crestron.Logos.SplusObjects.DigitalInput TV2SELECT;
        Crestron.Logos.SplusObjects.DigitalInput IPADDRESSSELECT;
        Crestron.Logos.SplusObjects.DigitalInput MACADDRESSSELECT;
        Crestron.Logos.SplusObjects.DigitalInput SAVE;
        Crestron.Logos.SplusObjects.StringInput IPADDRESSIN;
        Crestron.Logos.SplusObjects.StringInput MACADDRESSIN;
        Crestron.Logos.SplusObjects.DigitalOutput TV1SELECTED;
        Crestron.Logos.SplusObjects.DigitalOutput TV2SELECTED;
        Crestron.Logos.SplusObjects.DigitalOutput IPADDRESSSELECTED;
        Crestron.Logos.SplusObjects.DigitalOutput MACADDRESSSELECTED;
        Crestron.Logos.SplusObjects.StringOutput TV1IPADDRESSOUT;
        Crestron.Logos.SplusObjects.StringOutput TV1MACADDRESSOUT;
        Crestron.Logos.SplusObjects.StringOutput TV2IPADDRESSOUT;
        Crestron.Logos.SplusObjects.StringOutput TV2MACADDRESSOUT;
        ushort TVSELECTED = 0;
        ushort ADDRESSSELECTED = 0;
        object TV1SELECT_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 45;
                TVSELECTED = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 46;
                TV2SELECTED  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 47;
                TV1SELECTED  .Value = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 48;
                TV1IPADDRESSOUT  .UpdateValue ( _SplusNVRAM.TV1IPADDRESS  ) ; 
                __context__.SourceCodeLine = 49;
                TV1MACADDRESSOUT  .UpdateValue ( _SplusNVRAM.TV1MACADDRESS  ) ; 
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object TV2SELECT_OnPush_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 54;
            TVSELECTED = (ushort) ( 2 ) ; 
            __context__.SourceCodeLine = 55;
            TV1SELECTED  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 56;
            TV2SELECTED  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 57;
            TV2IPADDRESSOUT  .UpdateValue ( _SplusNVRAM.TV2IPADDRESS  ) ; 
            __context__.SourceCodeLine = 58;
            TV2MACADDRESSOUT  .UpdateValue ( _SplusNVRAM.TV2MACADDRESS  ) ; 
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object IPADDRESSSELECT_OnPush_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 64;
        ADDRESSSELECTED = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 65;
        MACADDRESSSELECTED  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 66;
        IPADDRESSSELECTED  .Value = (ushort) ( 1 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object MACADDRESSSELECT_OnPush_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 71;
        ADDRESSSELECTED = (ushort) ( 2 ) ; 
        __context__.SourceCodeLine = 72;
        IPADDRESSSELECTED  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 73;
        MACADDRESSSELECTED  .Value = (ushort) ( 1 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IPADDRESSIN_OnChange_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 78;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (TVSELECTED == 1) ) && Functions.TestForTrue ( Functions.BoolToInt (ADDRESSSELECTED == 1) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 80;
            _SplusNVRAM.TV1IPADDRESS  .UpdateValue ( IPADDRESSIN  ) ; 
            __context__.SourceCodeLine = 81;
            TV1IPADDRESSOUT  .UpdateValue ( _SplusNVRAM.TV1IPADDRESS  ) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 83;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (TVSELECTED == 2) ) && Functions.TestForTrue ( Functions.BoolToInt (ADDRESSSELECTED == 1) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 85;
                _SplusNVRAM.TV2IPADDRESS  .UpdateValue ( IPADDRESSIN  ) ; 
                __context__.SourceCodeLine = 86;
                TV2IPADDRESSOUT  .UpdateValue ( _SplusNVRAM.TV2IPADDRESS  ) ; 
                } 
            
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object MACADDRESSIN_OnChange_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 92;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (TVSELECTED == 1) ) && Functions.TestForTrue ( Functions.BoolToInt (ADDRESSSELECTED == 2) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 94;
            _SplusNVRAM.TV1MACADDRESS  .UpdateValue ( MACADDRESSIN  ) ; 
            __context__.SourceCodeLine = 95;
            TV1MACADDRESSOUT  .UpdateValue ( _SplusNVRAM.TV1MACADDRESS  ) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 97;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (TVSELECTED == 2) ) && Functions.TestForTrue ( Functions.BoolToInt (ADDRESSSELECTED == 2) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 99;
                _SplusNVRAM.TV2MACADDRESS  .UpdateValue ( MACADDRESSIN  ) ; 
                __context__.SourceCodeLine = 100;
                TV2MACADDRESSOUT  .UpdateValue ( _SplusNVRAM.TV2MACADDRESS  ) ; 
                } 
            
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 106;
        TVSELECTED = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 107;
        TV2SELECTED  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 108;
        TV1SELECTED  .Value = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 110;
        ADDRESSSELECTED = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 111;
        MACADDRESSSELECTED  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 112;
        IPADDRESSSELECTED  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 114;
        TV1IPADDRESSOUT  .UpdateValue ( _SplusNVRAM.TV1IPADDRESS  ) ; 
        __context__.SourceCodeLine = 115;
        TV2IPADDRESSOUT  .UpdateValue ( _SplusNVRAM.TV2IPADDRESS  ) ; 
        __context__.SourceCodeLine = 116;
        TV1MACADDRESSOUT  .UpdateValue ( _SplusNVRAM.TV1MACADDRESS  ) ; 
        __context__.SourceCodeLine = 117;
        TV2MACADDRESSOUT  .UpdateValue ( _SplusNVRAM.TV2MACADDRESS  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    SocketInfo __socketinfo__ = new SocketInfo( 1, this );
    InitialParametersClass.ResolveHostName = __socketinfo__.ResolveHostName;
    _SplusNVRAM = new SplusNVRAM( this );
    _SplusNVRAM.TV1IPADDRESS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
    _SplusNVRAM.TV2IPADDRESS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
    _SplusNVRAM.TV1MACADDRESS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 17, this );
    _SplusNVRAM.TV2MACADDRESS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 17, this );
    
    TV1SELECT = new Crestron.Logos.SplusObjects.DigitalInput( TV1SELECT__DigitalInput__, this );
    m_DigitalInputList.Add( TV1SELECT__DigitalInput__, TV1SELECT );
    
    TV2SELECT = new Crestron.Logos.SplusObjects.DigitalInput( TV2SELECT__DigitalInput__, this );
    m_DigitalInputList.Add( TV2SELECT__DigitalInput__, TV2SELECT );
    
    IPADDRESSSELECT = new Crestron.Logos.SplusObjects.DigitalInput( IPADDRESSSELECT__DigitalInput__, this );
    m_DigitalInputList.Add( IPADDRESSSELECT__DigitalInput__, IPADDRESSSELECT );
    
    MACADDRESSSELECT = new Crestron.Logos.SplusObjects.DigitalInput( MACADDRESSSELECT__DigitalInput__, this );
    m_DigitalInputList.Add( MACADDRESSSELECT__DigitalInput__, MACADDRESSSELECT );
    
    SAVE = new Crestron.Logos.SplusObjects.DigitalInput( SAVE__DigitalInput__, this );
    m_DigitalInputList.Add( SAVE__DigitalInput__, SAVE );
    
    TV1SELECTED = new Crestron.Logos.SplusObjects.DigitalOutput( TV1SELECTED__DigitalOutput__, this );
    m_DigitalOutputList.Add( TV1SELECTED__DigitalOutput__, TV1SELECTED );
    
    TV2SELECTED = new Crestron.Logos.SplusObjects.DigitalOutput( TV2SELECTED__DigitalOutput__, this );
    m_DigitalOutputList.Add( TV2SELECTED__DigitalOutput__, TV2SELECTED );
    
    IPADDRESSSELECTED = new Crestron.Logos.SplusObjects.DigitalOutput( IPADDRESSSELECTED__DigitalOutput__, this );
    m_DigitalOutputList.Add( IPADDRESSSELECTED__DigitalOutput__, IPADDRESSSELECTED );
    
    MACADDRESSSELECTED = new Crestron.Logos.SplusObjects.DigitalOutput( MACADDRESSSELECTED__DigitalOutput__, this );
    m_DigitalOutputList.Add( MACADDRESSSELECTED__DigitalOutput__, MACADDRESSSELECTED );
    
    IPADDRESSIN = new Crestron.Logos.SplusObjects.StringInput( IPADDRESSIN__AnalogSerialInput__, 15, this );
    m_StringInputList.Add( IPADDRESSIN__AnalogSerialInput__, IPADDRESSIN );
    
    MACADDRESSIN = new Crestron.Logos.SplusObjects.StringInput( MACADDRESSIN__AnalogSerialInput__, 17, this );
    m_StringInputList.Add( MACADDRESSIN__AnalogSerialInput__, MACADDRESSIN );
    
    TV1IPADDRESSOUT = new Crestron.Logos.SplusObjects.StringOutput( TV1IPADDRESSOUT__AnalogSerialOutput__, this );
    m_StringOutputList.Add( TV1IPADDRESSOUT__AnalogSerialOutput__, TV1IPADDRESSOUT );
    
    TV1MACADDRESSOUT = new Crestron.Logos.SplusObjects.StringOutput( TV1MACADDRESSOUT__AnalogSerialOutput__, this );
    m_StringOutputList.Add( TV1MACADDRESSOUT__AnalogSerialOutput__, TV1MACADDRESSOUT );
    
    TV2IPADDRESSOUT = new Crestron.Logos.SplusObjects.StringOutput( TV2IPADDRESSOUT__AnalogSerialOutput__, this );
    m_StringOutputList.Add( TV2IPADDRESSOUT__AnalogSerialOutput__, TV2IPADDRESSOUT );
    
    TV2MACADDRESSOUT = new Crestron.Logos.SplusObjects.StringOutput( TV2MACADDRESSOUT__AnalogSerialOutput__, this );
    m_StringOutputList.Add( TV2MACADDRESSOUT__AnalogSerialOutput__, TV2MACADDRESSOUT );
    
    
    TV1SELECT.OnDigitalPush.Add( new InputChangeHandlerWrapper( TV1SELECT_OnPush_0, false ) );
    TV2SELECT.OnDigitalPush.Add( new InputChangeHandlerWrapper( TV2SELECT_OnPush_1, false ) );
    IPADDRESSSELECT.OnDigitalPush.Add( new InputChangeHandlerWrapper( IPADDRESSSELECT_OnPush_2, false ) );
    MACADDRESSSELECT.OnDigitalPush.Add( new InputChangeHandlerWrapper( MACADDRESSSELECT_OnPush_3, false ) );
    IPADDRESSIN.OnSerialChange.Add( new InputChangeHandlerWrapper( IPADDRESSIN_OnChange_4, false ) );
    MACADDRESSIN.OnSerialChange.Add( new InputChangeHandlerWrapper( MACADDRESSIN_OnChange_5, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_2_TVS_IP_AND_MAC_SETUP ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}




const uint TV1SELECT__DigitalInput__ = 0;
const uint TV2SELECT__DigitalInput__ = 1;
const uint IPADDRESSSELECT__DigitalInput__ = 2;
const uint MACADDRESSSELECT__DigitalInput__ = 3;
const uint SAVE__DigitalInput__ = 4;
const uint IPADDRESSIN__AnalogSerialInput__ = 0;
const uint MACADDRESSIN__AnalogSerialInput__ = 1;
const uint TV1SELECTED__DigitalOutput__ = 0;
const uint TV2SELECTED__DigitalOutput__ = 1;
const uint IPADDRESSSELECTED__DigitalOutput__ = 2;
const uint MACADDRESSSELECTED__DigitalOutput__ = 3;
const uint TV1IPADDRESSOUT__AnalogSerialOutput__ = 0;
const uint TV1MACADDRESSOUT__AnalogSerialOutput__ = 1;
const uint TV2IPADDRESSOUT__AnalogSerialOutput__ = 2;
const uint TV2MACADDRESSOUT__AnalogSerialOutput__ = 3;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    [SplusStructAttribute(0, false, true)]
            public CrestronString TV1IPADDRESS;
            [SplusStructAttribute(1, false, true)]
            public CrestronString TV2IPADDRESS;
            [SplusStructAttribute(2, false, true)]
            public CrestronString TV1MACADDRESS;
            [SplusStructAttribute(3, false, true)]
            public CrestronString TV2MACADDRESS;
            
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
