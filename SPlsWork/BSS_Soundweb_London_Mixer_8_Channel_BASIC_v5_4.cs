using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_BSS_SOUNDWEB_LONDON_MIXER_8_CHANNEL_BASIC_V5_4
{
    public class UserModuleClass_BSS_SOUNDWEB_LONDON_MIXER_8_CHANNEL_BASIC_V5_4 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput ENABLE_FEEDBACK__DOLLAR__;
        Crestron.Logos.SplusObjects.DigitalInput DISABLE_FEEDBACK__DOLLAR__;
        Crestron.Logos.SplusObjects.DigitalInput CHAN1_MUTE__DOLLAR__;
        Crestron.Logos.SplusObjects.DigitalInput CHAN2_MUTE__DOLLAR__;
        Crestron.Logos.SplusObjects.DigitalInput CHAN3_MUTE__DOLLAR__;
        Crestron.Logos.SplusObjects.DigitalInput CHAN4_MUTE__DOLLAR__;
        Crestron.Logos.SplusObjects.DigitalInput CHAN5_MUTE__DOLLAR__;
        Crestron.Logos.SplusObjects.DigitalInput CHAN6_MUTE__DOLLAR__;
        Crestron.Logos.SplusObjects.DigitalInput CHAN7_MUTE__DOLLAR__;
        Crestron.Logos.SplusObjects.DigitalInput CHAN8_MUTE__DOLLAR__;
        Crestron.Logos.SplusObjects.DigitalInput MASTEROUT_MUTE__DOLLAR__;
        Crestron.Logos.SplusObjects.AnalogInput CHAN_OFFSET__DOLLAR__;
        Crestron.Logos.SplusObjects.AnalogInput CHAN1_VOL__DOLLAR__;
        Crestron.Logos.SplusObjects.AnalogInput CHAN2_VOL__DOLLAR__;
        Crestron.Logos.SplusObjects.AnalogInput CHAN3_VOL__DOLLAR__;
        Crestron.Logos.SplusObjects.AnalogInput CHAN4_VOL__DOLLAR__;
        Crestron.Logos.SplusObjects.AnalogInput CHAN5_VOL__DOLLAR__;
        Crestron.Logos.SplusObjects.AnalogInput CHAN6_VOL__DOLLAR__;
        Crestron.Logos.SplusObjects.AnalogInput CHAN7_VOL__DOLLAR__;
        Crestron.Logos.SplusObjects.AnalogInput CHAN8_VOL__DOLLAR__;
        Crestron.Logos.SplusObjects.AnalogInput MASTERVOL__DOLLAR__;
        Crestron.Logos.SplusObjects.StringInput RX__DOLLAR__;
        Crestron.Logos.SplusObjects.DigitalOutput CHAN1_MUTE_FB__DOLLAR__;
        Crestron.Logos.SplusObjects.DigitalOutput CHAN2_MUTE_FB__DOLLAR__;
        Crestron.Logos.SplusObjects.DigitalOutput CHAN3_MUTE_FB__DOLLAR__;
        Crestron.Logos.SplusObjects.DigitalOutput CHAN4_MUTE_FB__DOLLAR__;
        Crestron.Logos.SplusObjects.DigitalOutput CHAN5_MUTE_FB__DOLLAR__;
        Crestron.Logos.SplusObjects.DigitalOutput CHAN6_MUTE_FB__DOLLAR__;
        Crestron.Logos.SplusObjects.DigitalOutput CHAN7_MUTE_FB__DOLLAR__;
        Crestron.Logos.SplusObjects.DigitalOutput CHAN8_MUTE_FB__DOLLAR__;
        Crestron.Logos.SplusObjects.DigitalOutput MASTEROUT_MUTE_FB__DOLLAR__;
        Crestron.Logos.SplusObjects.AnalogOutput CHAN1_VOL_FB__DOLLAR__;
        Crestron.Logos.SplusObjects.AnalogOutput CHAN2_VOL_FB__DOLLAR__;
        Crestron.Logos.SplusObjects.AnalogOutput CHAN3_VOL_FB__DOLLAR__;
        Crestron.Logos.SplusObjects.AnalogOutput CHAN4_VOL_FB__DOLLAR__;
        Crestron.Logos.SplusObjects.AnalogOutput CHAN5_VOL_FB__DOLLAR__;
        Crestron.Logos.SplusObjects.AnalogOutput CHAN6_VOL_FB__DOLLAR__;
        Crestron.Logos.SplusObjects.AnalogOutput CHAN7_VOL_FB__DOLLAR__;
        Crestron.Logos.SplusObjects.AnalogOutput CHAN8_VOL_FB__DOLLAR__;
        Crestron.Logos.SplusObjects.AnalogOutput MASTERVOL_FB__DOLLAR__;
        Crestron.Logos.SplusObjects.StringOutput TX__DOLLAR__;
        StringParameter OBJECTID__DOLLAR__;
        ushort OFFSET = 0;
        ushort FEEDBACK = 0;
        ushort STATEVAR = 0;
        ushort SV = 0;
        ushort STATEVARSUB = 0;
        ushort STATEVARUNSUB = 0;
        ushort ITEMPNUM = 0;
        ushort IMARKER1 = 0;
        CrestronString RETURNSTRING;
        ushort RETURNI = 0;
        ushort CHAN1_VOL = 0;
        ushort CHAN2_VOL = 0;
        ushort CHAN3_VOL = 0;
        ushort CHAN4_VOL = 0;
        ushort CHAN5_VOL = 0;
        ushort CHAN6_VOL = 0;
        ushort CHAN7_VOL = 0;
        ushort CHAN8_VOL = 0;
        ushort MASTERVOL = 0;
        ushort XOKFEEDBACK = 0;
        ushort XOKCHAN1_VOL = 0;
        ushort XOKCHAN2_VOL = 0;
        ushort XOKCHAN3_VOL = 0;
        ushort XOKCHAN4_VOL = 0;
        ushort XOKCHAN5_VOL = 0;
        ushort XOKCHAN6_VOL = 0;
        ushort XOKCHAN7_VOL = 0;
        ushort XOKCHAN8_VOL = 0;
        ushort I = 0;
        ushort VOLUME = 0;
        ushort XOKMASTERVOL = 0;
        CrestronString TEMPSTRING;
        CrestronString STEMPSEND;
        CrestronString SOBJECTIDPADDED;
        CrestronString SNODE;
        private CrestronString SEND (  SplusExecutionContext __context__, CrestronString STR1 ) 
            { 
            ushort CHECKSUM = 0;
            ushort FIB = 0;
            
            CrestronString FSTEMPRETURN;
            CrestronString SENDSTRING;
            FSTEMPRETURN  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
            SENDSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
            
            
            __context__.SourceCodeLine = 89;
            CHECKSUM = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 90;
            SENDSTRING  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 91;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)Functions.Length( STR1 ); 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( FIB  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (FIB  >= __FN_FORSTART_VAL__1) && (FIB  <= __FN_FOREND_VAL__1) ) : ( (FIB  <= __FN_FORSTART_VAL__1) && (FIB  >= __FN_FOREND_VAL__1) ) ; FIB  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 93;
                CHECKSUM = (ushort) ( (CHECKSUM ^ Byte( STR1 , (int)( FIB ) )) ) ; 
                __context__.SourceCodeLine = 94;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (Byte( STR1 , (int)( FIB ) ) == 2) ) || Functions.TestForTrue ( Functions.BoolToInt (Byte( STR1 , (int)( FIB ) ) == 3) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (Byte( STR1 , (int)( FIB ) ) == 6) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (Byte( STR1 , (int)( FIB ) ) == 21) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (Byte( STR1 , (int)( FIB ) ) == 27) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 96;
                    MakeString ( SENDSTRING , "{0}\u001B{1}", SENDSTRING , Functions.Chr (  (int) ( (Byte( STR1 , (int)( FIB ) ) + 128) ) ) ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 100;
                    MakeString ( SENDSTRING , "{0}{1}", SENDSTRING , Functions.Chr (  (int) ( Byte( STR1 , (int)( FIB ) ) ) ) ) ; 
                    } 
                
                __context__.SourceCodeLine = 91;
                } 
            
            __context__.SourceCodeLine = 103;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (CHECKSUM == 2) ) || Functions.TestForTrue ( Functions.BoolToInt (CHECKSUM == 3) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (CHECKSUM == 6) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (CHECKSUM == 21) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (CHECKSUM == 27) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 105;
                MakeString ( FSTEMPRETURN , "\u0002{0}\u001B{1}\u0003", SENDSTRING , Functions.Chr (  (int) ( (CHECKSUM + 128) ) ) ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 109;
                MakeString ( FSTEMPRETURN , "\u0002{0}{1}\u0003", SENDSTRING , Functions.Chr (  (int) ( CHECKSUM ) ) ) ; 
                } 
            
            __context__.SourceCodeLine = 111;
            return ( FSTEMPRETURN ) ; 
            
            }
            
        private CrestronString ITOVOLUMEPERCENT (  SplusExecutionContext __context__, ushort INT ) 
            { 
            
            __context__.SourceCodeLine = 117;
            VOLUME = (ushort) ( ((INT * 100) / 65535) ) ; 
            __context__.SourceCodeLine = 118;
            RETURNSTRING  .UpdateValue ( "\u0000" + Functions.Chr (  (int) ( VOLUME ) ) + "\u0000\u0000"  ) ; 
            __context__.SourceCodeLine = 119;
            return ( RETURNSTRING ) ; 
            
            }
            
        private ushort VOLUMEPERCENTTOI (  SplusExecutionContext __context__, CrestronString STR ) 
            { 
            ushort FRACTION = 0;
            
            
            __context__.SourceCodeLine = 125;
            FRACTION = (ushort) ( ((Byte( STR , (int)( 3 ) ) * 256) + Byte( STR , (int)( 4 ) )) ) ; 
            __context__.SourceCodeLine = 126;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( FRACTION > 32767 ))  ) ) 
                { 
                __context__.SourceCodeLine = 128;
                VOLUME = (ushort) ( (((Byte( STR , (int)( 2 ) ) + 1) * 65535) / 100) ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 132;
                VOLUME = (ushort) ( ((Byte( STR , (int)( 2 ) ) * 65535) / 100) ) ; 
                } 
            
            __context__.SourceCodeLine = 134;
            RETURNI = (ushort) ( VOLUME ) ; 
            __context__.SourceCodeLine = 136;
            return (ushort)( RETURNI) ; 
            
            }
            
        private CrestronString FPAD (  SplusExecutionContext __context__, CrestronString FSPAD ) 
            { 
            ushort FIA = 0;
            
            CrestronString FSRETURN;
            FSRETURN  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 10, this );
            
            
            __context__.SourceCodeLine = 145;
            FSRETURN  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 146;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)Functions.Length( FSPAD ); 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( FIA  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (FIA  >= __FN_FORSTART_VAL__1) && (FIA  <= __FN_FOREND_VAL__1) ) : ( (FIA  <= __FN_FORSTART_VAL__1) && (FIA  >= __FN_FOREND_VAL__1) ) ; FIA  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 148;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (Byte( FSPAD , (int)( FIA ) ) == 2) ) || Functions.TestForTrue ( Functions.BoolToInt (Byte( FSPAD , (int)( FIA ) ) == 3) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (Byte( FSPAD , (int)( FIA ) ) == 6) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (Byte( FSPAD , (int)( FIA ) ) == 21) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (Byte( FSPAD , (int)( FIA ) ) == 27) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 150;
                    MakeString ( FSRETURN , "{0}\u001B{1}", FSRETURN , Functions.Chr (  (int) ( (Byte( FSPAD , (int)( FIA ) ) + 128) ) ) ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 154;
                    MakeString ( FSRETURN , "{0}{1}", FSRETURN , Functions.Chr (  (int) ( Byte( FSPAD , (int)( FIA ) ) ) ) ) ; 
                    } 
                
                __context__.SourceCodeLine = 146;
                } 
            
            __context__.SourceCodeLine = 157;
            return ( FSRETURN ) ; 
            
            }
            
        private CrestronString FUNPAD (  SplusExecutionContext __context__, CrestronString STR2 ) 
            { 
            ushort J = 0;
            
            CrestronString RECEIVESTRING;
            RECEIVESTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
            
            
            __context__.SourceCodeLine = 165;
            RECEIVESTRING  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 166;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)Functions.Length( STR2 ); 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( J  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (J  >= __FN_FORSTART_VAL__1) && (J  <= __FN_FOREND_VAL__1) ) : ( (J  <= __FN_FORSTART_VAL__1) && (J  >= __FN_FOREND_VAL__1) ) ; J  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 168;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Byte( STR2 , (int)( J ) ) == 27))  ) ) 
                    { 
                    __context__.SourceCodeLine = 170;
                    RECEIVESTRING  .UpdateValue ( RECEIVESTRING + Functions.Chr (  (int) ( (Byte( STR2 , (int)( (J + 1) ) ) - 128) ) )  ) ; 
                    __context__.SourceCodeLine = 171;
                    J = (ushort) ( (J + 1) ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 175;
                    RECEIVESTRING  .UpdateValue ( RECEIVESTRING + Functions.Chr (  (int) ( Byte( STR2 , (int)( J ) ) ) )  ) ; 
                    } 
                
                __context__.SourceCodeLine = 166;
                } 
            
            __context__.SourceCodeLine = 178;
            while ( Functions.TestForTrue  ( ( Functions.BoolToInt (Byte( RECEIVESTRING , (int)( 1 ) ) == 6))  ) ) 
                { 
                __context__.SourceCodeLine = 180;
                RECEIVESTRING  .UpdateValue ( Functions.Right ( RECEIVESTRING ,  (int) ( (Functions.Length( RECEIVESTRING ) - 1) ) )  ) ; 
                __context__.SourceCodeLine = 178;
                } 
            
            __context__.SourceCodeLine = 182;
            return ( RECEIVESTRING ) ; 
            
            }
            
        object CHAN_OFFSET__DOLLAR___OnChange_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 190;
                OFFSET = (ushort) ( (CHAN_OFFSET__DOLLAR__  .UshortValue * 8) ) ; 
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object ENABLE_FEEDBACK__DOLLAR___OnPush_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 195;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_7__" , 20 , __SPLS_TMPVAR__WAITLABEL_7___Callback ) ;
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
public void __SPLS_TMPVAR__WAITLABEL_7___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 197;
            if ( Functions.TestForTrue  ( ( XOKFEEDBACK)  ) ) 
                { 
                __context__.SourceCodeLine = 199;
                XOKFEEDBACK = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 200;
                FEEDBACK = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 202;
                ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__1 = (ushort)8; 
                int __FN_FORSTEP_VAL__1 = (int)1; 
                for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                    { 
                    __context__.SourceCodeLine = 204;
                    STATEVARSUB = (ushort) ( ((((I + OFFSET) - 1) * 100) + 1) ) ; 
                    __context__.SourceCodeLine = 205;
                    MakeString ( STEMPSEND , "\u0089{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVARSUB ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVARSUB ) ) ) ) ; 
                    __context__.SourceCodeLine = 206;
                    TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
                    __context__.SourceCodeLine = 207;
                    STATEVARSUB = (ushort) ( (((I + OFFSET) - 1) * 100) ) ; 
                    __context__.SourceCodeLine = 208;
                    MakeString ( STEMPSEND , "\u008E{0}\u0003{1}{2}{3}{4}", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVARSUB ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVARSUB ) ) ) , "\u0000\u0000\u0000\u0000" ) ; 
                    __context__.SourceCodeLine = 209;
                    TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
                    __context__.SourceCodeLine = 202;
                    } 
                
                __context__.SourceCodeLine = 212;
                STATEVARSUB = (ushort) ( 20001 ) ; 
                __context__.SourceCodeLine = 213;
                MakeString ( STEMPSEND , "\u0089{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVARSUB ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVARSUB ) ) ) ) ; 
                __context__.SourceCodeLine = 214;
                TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
                __context__.SourceCodeLine = 215;
                STATEVARSUB = (ushort) ( 20000 ) ; 
                __context__.SourceCodeLine = 216;
                MakeString ( STEMPSEND , "\u008E{0}\u0003{1}{2}{3}{4}", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVARSUB ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVARSUB ) ) ) , "\u0000\u0000\u0000\u0000" ) ; 
                __context__.SourceCodeLine = 217;
                TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
                __context__.SourceCodeLine = 218;
                XOKFEEDBACK = (ushort) ( 1 ) ; 
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object DISABLE_FEEDBACK__DOLLAR___OnPush_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 225;
        if ( Functions.TestForTrue  ( ( XOKFEEDBACK)  ) ) 
            { 
            __context__.SourceCodeLine = 227;
            XOKFEEDBACK = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 228;
            FEEDBACK = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 230;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)8; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 232;
                STATEVARSUB = (ushort) ( ((((I + OFFSET) - 1) * 100) + 1) ) ; 
                __context__.SourceCodeLine = 233;
                MakeString ( STEMPSEND , "\u008A{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVARSUB ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVARSUB ) ) ) ) ; 
                __context__.SourceCodeLine = 234;
                TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
                __context__.SourceCodeLine = 235;
                STATEVARSUB = (ushort) ( (((I + OFFSET) - 1) * 100) ) ; 
                __context__.SourceCodeLine = 236;
                MakeString ( STEMPSEND , "\u008F{0}\u0003{1}{2}{3}{4}", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVARSUB ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVARSUB ) ) ) , "\u0000\u0000\u0000\u0000" ) ; 
                __context__.SourceCodeLine = 237;
                TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
                __context__.SourceCodeLine = 230;
                } 
            
            __context__.SourceCodeLine = 240;
            STATEVARSUB = (ushort) ( 20001 ) ; 
            __context__.SourceCodeLine = 241;
            MakeString ( STEMPSEND , "\u008A{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVARSUB ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVARSUB ) ) ) ) ; 
            __context__.SourceCodeLine = 242;
            TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
            __context__.SourceCodeLine = 243;
            STATEVARSUB = (ushort) ( 20000 ) ; 
            __context__.SourceCodeLine = 244;
            MakeString ( STEMPSEND , "\u008F{0}\u0003{1}{2}{3}{4}", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVARSUB ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVARSUB ) ) ) , "\u0000\u0000\u0000\u0000" ) ; 
            __context__.SourceCodeLine = 245;
            TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
            __context__.SourceCodeLine = 246;
            XOKFEEDBACK = (ushort) ( 1 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHAN1_MUTE__DOLLAR___OnPush_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 252;
        STATEVAR = (ushort) ( ((((1 + OFFSET) - 1) * 100) + 1) ) ; 
        __context__.SourceCodeLine = 253;
        MakeString ( STEMPSEND , "\u0088{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0001", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
        __context__.SourceCodeLine = 254;
        TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
        __context__.SourceCodeLine = 256;
        if ( Functions.TestForTrue  ( ( FEEDBACK)  ) ) 
            { 
            __context__.SourceCodeLine = 258;
            MakeString ( STEMPSEND , "\u0089{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
            __context__.SourceCodeLine = 259;
            TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHAN1_MUTE__DOLLAR___OnRelease_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 265;
        STATEVAR = (ushort) ( ((((1 + OFFSET) - 1) * 100) + 1) ) ; 
        __context__.SourceCodeLine = 266;
        MakeString ( STEMPSEND , "\u0088{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
        __context__.SourceCodeLine = 267;
        TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
        __context__.SourceCodeLine = 269;
        if ( Functions.TestForTrue  ( ( FEEDBACK)  ) ) 
            { 
            __context__.SourceCodeLine = 271;
            MakeString ( STEMPSEND , "\u0089{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
            __context__.SourceCodeLine = 272;
            TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHAN2_MUTE__DOLLAR___OnPush_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 278;
        STATEVAR = (ushort) ( ((((2 + OFFSET) - 1) * 100) + 1) ) ; 
        __context__.SourceCodeLine = 279;
        MakeString ( STEMPSEND , "\u0088{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0001", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
        __context__.SourceCodeLine = 280;
        TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
        __context__.SourceCodeLine = 282;
        if ( Functions.TestForTrue  ( ( FEEDBACK)  ) ) 
            { 
            __context__.SourceCodeLine = 284;
            MakeString ( STEMPSEND , "\u0089{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
            __context__.SourceCodeLine = 285;
            TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHAN2_MUTE__DOLLAR___OnRelease_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 291;
        STATEVAR = (ushort) ( ((((2 + OFFSET) - 1) * 100) + 1) ) ; 
        __context__.SourceCodeLine = 292;
        MakeString ( STEMPSEND , "\u0088{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
        __context__.SourceCodeLine = 293;
        TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
        __context__.SourceCodeLine = 295;
        if ( Functions.TestForTrue  ( ( FEEDBACK)  ) ) 
            { 
            __context__.SourceCodeLine = 297;
            MakeString ( STEMPSEND , "\u0089{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
            __context__.SourceCodeLine = 298;
            TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHAN3_MUTE__DOLLAR___OnPush_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 304;
        STATEVAR = (ushort) ( ((((3 + OFFSET) - 1) * 100) + 1) ) ; 
        __context__.SourceCodeLine = 305;
        MakeString ( STEMPSEND , "\u0088{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0001", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
        __context__.SourceCodeLine = 306;
        TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
        __context__.SourceCodeLine = 308;
        if ( Functions.TestForTrue  ( ( FEEDBACK)  ) ) 
            { 
            __context__.SourceCodeLine = 310;
            MakeString ( STEMPSEND , "\u0089{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
            __context__.SourceCodeLine = 311;
            TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHAN3_MUTE__DOLLAR___OnRelease_8 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 317;
        STATEVAR = (ushort) ( ((((3 + OFFSET) - 1) * 100) + 1) ) ; 
        __context__.SourceCodeLine = 318;
        MakeString ( STEMPSEND , "\u0088{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
        __context__.SourceCodeLine = 319;
        TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
        __context__.SourceCodeLine = 321;
        if ( Functions.TestForTrue  ( ( FEEDBACK)  ) ) 
            { 
            __context__.SourceCodeLine = 323;
            MakeString ( STEMPSEND , "\u0089{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
            __context__.SourceCodeLine = 324;
            TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHAN4_MUTE__DOLLAR___OnPush_9 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 330;
        STATEVAR = (ushort) ( ((((4 + OFFSET) - 1) * 100) + 1) ) ; 
        __context__.SourceCodeLine = 331;
        MakeString ( STEMPSEND , "\u0088{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0001", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
        __context__.SourceCodeLine = 332;
        TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
        __context__.SourceCodeLine = 334;
        if ( Functions.TestForTrue  ( ( FEEDBACK)  ) ) 
            { 
            __context__.SourceCodeLine = 336;
            MakeString ( STEMPSEND , "\u0089{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
            __context__.SourceCodeLine = 337;
            TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHAN4_MUTE__DOLLAR___OnRelease_10 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 343;
        STATEVAR = (ushort) ( ((((4 + OFFSET) - 1) * 100) + 1) ) ; 
        __context__.SourceCodeLine = 344;
        MakeString ( STEMPSEND , "\u0088{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
        __context__.SourceCodeLine = 345;
        TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
        __context__.SourceCodeLine = 347;
        if ( Functions.TestForTrue  ( ( FEEDBACK)  ) ) 
            { 
            __context__.SourceCodeLine = 349;
            MakeString ( STEMPSEND , "\u0089{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
            __context__.SourceCodeLine = 350;
            TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHAN5_MUTE__DOLLAR___OnPush_11 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 356;
        STATEVAR = (ushort) ( ((((5 + OFFSET) - 1) * 100) + 1) ) ; 
        __context__.SourceCodeLine = 357;
        MakeString ( STEMPSEND , "\u0088{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0001", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
        __context__.SourceCodeLine = 358;
        TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
        __context__.SourceCodeLine = 360;
        if ( Functions.TestForTrue  ( ( FEEDBACK)  ) ) 
            { 
            __context__.SourceCodeLine = 362;
            MakeString ( STEMPSEND , "\u0089{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
            __context__.SourceCodeLine = 363;
            TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHAN5_MUTE__DOLLAR___OnRelease_12 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 369;
        STATEVAR = (ushort) ( ((((5 + OFFSET) - 1) * 100) + 1) ) ; 
        __context__.SourceCodeLine = 370;
        MakeString ( STEMPSEND , "\u0088{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
        __context__.SourceCodeLine = 371;
        TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
        __context__.SourceCodeLine = 373;
        if ( Functions.TestForTrue  ( ( FEEDBACK)  ) ) 
            { 
            __context__.SourceCodeLine = 375;
            MakeString ( STEMPSEND , "\u0089{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
            __context__.SourceCodeLine = 376;
            TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHAN6_MUTE__DOLLAR___OnPush_13 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 382;
        STATEVAR = (ushort) ( ((((6 + OFFSET) - 1) * 100) + 1) ) ; 
        __context__.SourceCodeLine = 383;
        MakeString ( STEMPSEND , "\u0088{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0001", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
        __context__.SourceCodeLine = 384;
        TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
        __context__.SourceCodeLine = 386;
        if ( Functions.TestForTrue  ( ( FEEDBACK)  ) ) 
            { 
            __context__.SourceCodeLine = 388;
            MakeString ( STEMPSEND , "\u0089{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
            __context__.SourceCodeLine = 389;
            TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHAN6_MUTE__DOLLAR___OnRelease_14 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 395;
        STATEVAR = (ushort) ( ((((6 + OFFSET) - 1) * 100) + 1) ) ; 
        __context__.SourceCodeLine = 396;
        MakeString ( STEMPSEND , "\u0088{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
        __context__.SourceCodeLine = 397;
        TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
        __context__.SourceCodeLine = 399;
        if ( Functions.TestForTrue  ( ( FEEDBACK)  ) ) 
            { 
            __context__.SourceCodeLine = 401;
            MakeString ( STEMPSEND , "\u0089{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
            __context__.SourceCodeLine = 402;
            TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHAN7_MUTE__DOLLAR___OnPush_15 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 408;
        STATEVAR = (ushort) ( ((((7 + OFFSET) - 1) * 100) + 1) ) ; 
        __context__.SourceCodeLine = 409;
        MakeString ( STEMPSEND , "\u0088{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0001", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
        __context__.SourceCodeLine = 410;
        TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
        __context__.SourceCodeLine = 412;
        if ( Functions.TestForTrue  ( ( FEEDBACK)  ) ) 
            { 
            __context__.SourceCodeLine = 414;
            MakeString ( STEMPSEND , "\u0089{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
            __context__.SourceCodeLine = 415;
            TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHAN7_MUTE__DOLLAR___OnRelease_16 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 421;
        STATEVAR = (ushort) ( ((((7 + OFFSET) - 1) * 100) + 1) ) ; 
        __context__.SourceCodeLine = 422;
        MakeString ( STEMPSEND , "\u0088{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
        __context__.SourceCodeLine = 423;
        TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
        __context__.SourceCodeLine = 425;
        if ( Functions.TestForTrue  ( ( FEEDBACK)  ) ) 
            { 
            __context__.SourceCodeLine = 427;
            MakeString ( STEMPSEND , "\u0089{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
            __context__.SourceCodeLine = 428;
            TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHAN8_MUTE__DOLLAR___OnPush_17 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 434;
        STATEVAR = (ushort) ( ((((8 + OFFSET) - 1) * 100) + 1) ) ; 
        __context__.SourceCodeLine = 435;
        MakeString ( STEMPSEND , "\u0088{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0001", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
        __context__.SourceCodeLine = 436;
        TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
        __context__.SourceCodeLine = 438;
        if ( Functions.TestForTrue  ( ( FEEDBACK)  ) ) 
            { 
            __context__.SourceCodeLine = 440;
            MakeString ( STEMPSEND , "\u0089{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
            __context__.SourceCodeLine = 441;
            TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHAN8_MUTE__DOLLAR___OnRelease_18 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 447;
        STATEVAR = (ushort) ( ((((8 + OFFSET) - 1) * 100) + 1) ) ; 
        __context__.SourceCodeLine = 448;
        MakeString ( STEMPSEND , "\u0088{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
        __context__.SourceCodeLine = 449;
        TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
        __context__.SourceCodeLine = 451;
        if ( Functions.TestForTrue  ( ( FEEDBACK)  ) ) 
            { 
            __context__.SourceCodeLine = 453;
            MakeString ( STEMPSEND , "\u0089{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
            __context__.SourceCodeLine = 454;
            TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object MASTEROUT_MUTE__DOLLAR___OnPush_19 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 460;
        STATEVAR = (ushort) ( 20001 ) ; 
        __context__.SourceCodeLine = 461;
        MakeString ( STEMPSEND , "\u0088{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0001", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
        __context__.SourceCodeLine = 462;
        TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
        __context__.SourceCodeLine = 464;
        if ( Functions.TestForTrue  ( ( FEEDBACK)  ) ) 
            { 
            __context__.SourceCodeLine = 466;
            MakeString ( STEMPSEND , "\u0089{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
            __context__.SourceCodeLine = 467;
            TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object MASTEROUT_MUTE__DOLLAR___OnRelease_20 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 473;
        STATEVAR = (ushort) ( 20001 ) ; 
        __context__.SourceCodeLine = 474;
        MakeString ( STEMPSEND , "\u0088{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
        __context__.SourceCodeLine = 475;
        TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
        __context__.SourceCodeLine = 477;
        if ( Functions.TestForTrue  ( ( FEEDBACK)  ) ) 
            { 
            __context__.SourceCodeLine = 479;
            MakeString ( STEMPSEND , "\u0089{0}\u0003{1}{2}{3}\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) ) ; 
            __context__.SourceCodeLine = 480;
            TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHAN1_VOL__DOLLAR___OnChange_21 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 486;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CHAN1_VOL != CHAN1_VOL__DOLLAR__  .UshortValue))  ) ) 
            { 
            __context__.SourceCodeLine = 488;
            if ( Functions.TestForTrue  ( ( XOKCHAN1_VOL)  ) ) 
                { 
                __context__.SourceCodeLine = 490;
                XOKCHAN1_VOL = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 491;
                STATEVAR = (ushort) ( (((1 + OFFSET) - 1) * 100) ) ; 
                __context__.SourceCodeLine = 492;
                CHAN1_VOL = (ushort) ( CHAN1_VOL__DOLLAR__  .UshortValue ) ; 
                __context__.SourceCodeLine = 493;
                CHAN1_VOL_FB__DOLLAR__  .Value = (ushort) ( CHAN1_VOL__DOLLAR__  .UshortValue ) ; 
                __context__.SourceCodeLine = 494;
                MakeString ( STEMPSEND , "\u008D{0}\u0003{1}{2}{3}{4}", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) , ITOVOLUMEPERCENT (  __context__ , (ushort)( CHAN1_VOL__DOLLAR__  .UshortValue )) ) ; 
                __context__.SourceCodeLine = 495;
                TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
                __context__.SourceCodeLine = 496;
                XOKCHAN1_VOL = (ushort) ( 1 ) ; 
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHAN2_VOL__DOLLAR___OnChange_22 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 503;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CHAN2_VOL != CHAN2_VOL__DOLLAR__  .UshortValue))  ) ) 
            { 
            __context__.SourceCodeLine = 505;
            if ( Functions.TestForTrue  ( ( XOKCHAN2_VOL)  ) ) 
                { 
                __context__.SourceCodeLine = 507;
                XOKCHAN2_VOL = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 508;
                CHAN2_VOL = (ushort) ( CHAN2_VOL__DOLLAR__  .UshortValue ) ; 
                __context__.SourceCodeLine = 509;
                CHAN2_VOL_FB__DOLLAR__  .Value = (ushort) ( CHAN2_VOL__DOLLAR__  .UshortValue ) ; 
                __context__.SourceCodeLine = 510;
                STATEVAR = (ushort) ( (((2 + OFFSET) - 1) * 100) ) ; 
                __context__.SourceCodeLine = 511;
                MakeString ( STEMPSEND , "\u008D{0}\u0003{1}{2}{3}{4}", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) , ITOVOLUMEPERCENT (  __context__ , (ushort)( CHAN2_VOL__DOLLAR__  .UshortValue )) ) ; 
                __context__.SourceCodeLine = 512;
                TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
                __context__.SourceCodeLine = 513;
                XOKCHAN2_VOL = (ushort) ( 1 ) ; 
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHAN3_VOL__DOLLAR___OnChange_23 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 520;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CHAN3_VOL != CHAN3_VOL__DOLLAR__  .UshortValue))  ) ) 
            { 
            __context__.SourceCodeLine = 522;
            if ( Functions.TestForTrue  ( ( XOKCHAN3_VOL)  ) ) 
                { 
                __context__.SourceCodeLine = 524;
                XOKCHAN3_VOL = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 525;
                CHAN3_VOL = (ushort) ( CHAN3_VOL__DOLLAR__  .UshortValue ) ; 
                __context__.SourceCodeLine = 526;
                CHAN3_VOL_FB__DOLLAR__  .Value = (ushort) ( CHAN3_VOL__DOLLAR__  .UshortValue ) ; 
                __context__.SourceCodeLine = 527;
                STATEVAR = (ushort) ( (((3 + OFFSET) - 1) * 100) ) ; 
                __context__.SourceCodeLine = 528;
                MakeString ( STEMPSEND , "\u008D{0}\u0003{1}{2}{3}{4}", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) , ITOVOLUMEPERCENT (  __context__ , (ushort)( CHAN3_VOL__DOLLAR__  .UshortValue )) ) ; 
                __context__.SourceCodeLine = 529;
                TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
                __context__.SourceCodeLine = 530;
                XOKCHAN3_VOL = (ushort) ( 1 ) ; 
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHAN4_VOL__DOLLAR___OnChange_24 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 537;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CHAN4_VOL != CHAN4_VOL__DOLLAR__  .UshortValue))  ) ) 
            { 
            __context__.SourceCodeLine = 539;
            if ( Functions.TestForTrue  ( ( XOKCHAN4_VOL)  ) ) 
                { 
                __context__.SourceCodeLine = 541;
                XOKCHAN4_VOL = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 542;
                CHAN4_VOL = (ushort) ( CHAN4_VOL__DOLLAR__  .UshortValue ) ; 
                __context__.SourceCodeLine = 543;
                CHAN4_VOL_FB__DOLLAR__  .Value = (ushort) ( CHAN4_VOL__DOLLAR__  .UshortValue ) ; 
                __context__.SourceCodeLine = 544;
                STATEVAR = (ushort) ( (((4 + OFFSET) - 1) * 100) ) ; 
                __context__.SourceCodeLine = 545;
                MakeString ( STEMPSEND , "\u008D{0}\u0003{1}{2}{3}{4}", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) , ITOVOLUMEPERCENT (  __context__ , (ushort)( CHAN4_VOL__DOLLAR__  .UshortValue )) ) ; 
                __context__.SourceCodeLine = 546;
                TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
                __context__.SourceCodeLine = 547;
                XOKCHAN4_VOL = (ushort) ( 1 ) ; 
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHAN5_VOL__DOLLAR___OnChange_25 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 554;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CHAN5_VOL != CHAN5_VOL__DOLLAR__  .UshortValue))  ) ) 
            { 
            __context__.SourceCodeLine = 556;
            if ( Functions.TestForTrue  ( ( XOKCHAN5_VOL)  ) ) 
                { 
                __context__.SourceCodeLine = 558;
                XOKCHAN5_VOL = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 559;
                CHAN5_VOL = (ushort) ( CHAN5_VOL__DOLLAR__  .UshortValue ) ; 
                __context__.SourceCodeLine = 560;
                CHAN5_VOL_FB__DOLLAR__  .Value = (ushort) ( CHAN5_VOL__DOLLAR__  .UshortValue ) ; 
                __context__.SourceCodeLine = 561;
                STATEVAR = (ushort) ( (((5 + OFFSET) - 1) * 100) ) ; 
                __context__.SourceCodeLine = 562;
                MakeString ( STEMPSEND , "\u008D{0}\u0003{1}{2}{3}{4}", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) , ITOVOLUMEPERCENT (  __context__ , (ushort)( CHAN5_VOL__DOLLAR__  .UshortValue )) ) ; 
                __context__.SourceCodeLine = 563;
                TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
                __context__.SourceCodeLine = 564;
                XOKCHAN5_VOL = (ushort) ( 1 ) ; 
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHAN6_VOL__DOLLAR___OnChange_26 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 571;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CHAN6_VOL != CHAN6_VOL__DOLLAR__  .UshortValue))  ) ) 
            { 
            __context__.SourceCodeLine = 573;
            if ( Functions.TestForTrue  ( ( XOKCHAN6_VOL)  ) ) 
                { 
                __context__.SourceCodeLine = 575;
                XOKCHAN6_VOL = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 576;
                CHAN6_VOL = (ushort) ( CHAN6_VOL__DOLLAR__  .UshortValue ) ; 
                __context__.SourceCodeLine = 577;
                CHAN6_VOL_FB__DOLLAR__  .Value = (ushort) ( CHAN6_VOL__DOLLAR__  .UshortValue ) ; 
                __context__.SourceCodeLine = 578;
                STATEVAR = (ushort) ( (((6 + OFFSET) - 1) * 100) ) ; 
                __context__.SourceCodeLine = 579;
                MakeString ( STEMPSEND , "\u008D{0}\u0003{1}{2}{3}{4}", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) , ITOVOLUMEPERCENT (  __context__ , (ushort)( CHAN6_VOL__DOLLAR__  .UshortValue )) ) ; 
                __context__.SourceCodeLine = 580;
                TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
                __context__.SourceCodeLine = 581;
                XOKCHAN6_VOL = (ushort) ( 1 ) ; 
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHAN7_VOL__DOLLAR___OnChange_27 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 588;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CHAN7_VOL != CHAN7_VOL__DOLLAR__  .UshortValue))  ) ) 
            { 
            __context__.SourceCodeLine = 590;
            if ( Functions.TestForTrue  ( ( XOKCHAN7_VOL)  ) ) 
                { 
                __context__.SourceCodeLine = 592;
                XOKCHAN7_VOL = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 593;
                CHAN7_VOL = (ushort) ( CHAN7_VOL__DOLLAR__  .UshortValue ) ; 
                __context__.SourceCodeLine = 594;
                CHAN7_VOL_FB__DOLLAR__  .Value = (ushort) ( CHAN7_VOL__DOLLAR__  .UshortValue ) ; 
                __context__.SourceCodeLine = 595;
                STATEVAR = (ushort) ( (((7 + OFFSET) - 1) * 100) ) ; 
                __context__.SourceCodeLine = 596;
                MakeString ( STEMPSEND , "\u008D{0}\u0003{1}{2}{3}{4}", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) , ITOVOLUMEPERCENT (  __context__ , (ushort)( CHAN7_VOL__DOLLAR__  .UshortValue )) ) ; 
                __context__.SourceCodeLine = 597;
                TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
                __context__.SourceCodeLine = 598;
                XOKCHAN7_VOL = (ushort) ( 1 ) ; 
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHAN8_VOL__DOLLAR___OnChange_28 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 605;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CHAN8_VOL != CHAN8_VOL__DOLLAR__  .UshortValue))  ) ) 
            { 
            __context__.SourceCodeLine = 607;
            if ( Functions.TestForTrue  ( ( XOKCHAN8_VOL)  ) ) 
                { 
                __context__.SourceCodeLine = 609;
                XOKCHAN8_VOL = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 610;
                CHAN8_VOL = (ushort) ( CHAN8_VOL__DOLLAR__  .UshortValue ) ; 
                __context__.SourceCodeLine = 611;
                CHAN8_VOL_FB__DOLLAR__  .Value = (ushort) ( CHAN8_VOL__DOLLAR__  .UshortValue ) ; 
                __context__.SourceCodeLine = 612;
                STATEVAR = (ushort) ( (((8 + OFFSET) - 1) * 100) ) ; 
                __context__.SourceCodeLine = 613;
                MakeString ( STEMPSEND , "\u008D{0}\u0003{1}{2}{3}{4}", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) , ITOVOLUMEPERCENT (  __context__ , (ushort)( CHAN8_VOL__DOLLAR__  .UshortValue )) ) ; 
                __context__.SourceCodeLine = 614;
                TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
                __context__.SourceCodeLine = 615;
                XOKCHAN8_VOL = (ushort) ( 1 ) ; 
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object MASTERVOL__DOLLAR___OnChange_29 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 622;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (MASTERVOL != MASTERVOL__DOLLAR__  .UshortValue))  ) ) 
            { 
            __context__.SourceCodeLine = 624;
            if ( Functions.TestForTrue  ( ( XOKMASTERVOL)  ) ) 
                { 
                __context__.SourceCodeLine = 626;
                XOKMASTERVOL = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 627;
                MASTERVOL = (ushort) ( MASTERVOL__DOLLAR__  .UshortValue ) ; 
                __context__.SourceCodeLine = 628;
                MASTERVOL_FB__DOLLAR__  .Value = (ushort) ( MASTERVOL__DOLLAR__  .UshortValue ) ; 
                __context__.SourceCodeLine = 629;
                STATEVAR = (ushort) ( 20000 ) ; 
                __context__.SourceCodeLine = 630;
                MakeString ( STEMPSEND , "\u008D{0}\u0003{1}{2}{3}{4}", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( Functions.High( (ushort) STATEVAR ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) STATEVAR ) ) ) , ITOVOLUMEPERCENT (  __context__ , (ushort)( MASTERVOL__DOLLAR__  .UshortValue )) ) ; 
                __context__.SourceCodeLine = 631;
                TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
                __context__.SourceCodeLine = 632;
                XOKMASTERVOL = (ushort) ( 1 ) ; 
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

private void PROCESSRXMESSAGE (  SplusExecutionContext __context__, CrestronString PARAMDATA ) 
    { 
    
    __context__.SourceCodeLine = 639;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Find( "Send Name" , PARAMDATA ) > 0 ))  ) ) 
        { 
        __context__.SourceCodeLine = 641;
        ITEMPNUM = (ushort) ( Functions.Atoi( PARAMDATA ) ) ; 
        __context__.SourceCodeLine = 642;
        IMARKER1 = (ushort) ( (Functions.Find( "Node " , PARAMDATA ) + 5) ) ; 
        __context__.SourceCodeLine = 643;
        SNODE  .UpdateValue ( Functions.Mid ( PARAMDATA ,  (int) ( IMARKER1 ) ,  (int) ( 2 ) )  ) ; 
        __context__.SourceCodeLine = 644;
        SOBJECTIDPADDED  .UpdateValue ( FPAD (  __context__ , OBJECTID__DOLLAR__ )  ) ; 
        __context__.SourceCodeLine = 645;
        MakeString ( TX__DOLLAR__ , "Send Name{0:d} = {1}\u0003\u0003\u0003\u0003\u0003", (short)ITEMPNUM, SOBJECTIDPADDED ) ; 
        } 
    
    else 
        {
        __context__.SourceCodeLine = 647;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (Functions.Mid( PARAMDATA , (int)( 7 ) , (int)( 3 ) ) == "\u0000\u0000\u0000") ) || Functions.TestForTrue ( Functions.BoolToInt (Functions.Mid( PARAMDATA , (int)( 7 ) , (int)( Functions.Length( SOBJECTIDPADDED ) ) ) == SOBJECTIDPADDED) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 649;
            PARAMDATA  .UpdateValue ( FUNPAD (  __context__ , PARAMDATA)  ) ; 
            __context__.SourceCodeLine = 650;
            SV = (ushort) ( ((Byte( PARAMDATA , (int)( 9 ) ) * 256) + Byte( PARAMDATA , (int)( 10 ) )) ) ; 
            __context__.SourceCodeLine = 651;
            if ( Functions.TestForTrue  ( ( Mod( SV , 100 ))  ) ) 
                { 
                __context__.SourceCodeLine = 653;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (((((1 + OFFSET) - 1) * 100) + 1) == SV))  ) ) 
                    {
                    __context__.SourceCodeLine = 654;
                    CHAN1_MUTE_FB__DOLLAR__  .Value = (ushort) ( Byte( PARAMDATA , (int)( 14 ) ) ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 655;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (((((2 + OFFSET) - 1) * 100) + 1) == SV))  ) ) 
                        {
                        __context__.SourceCodeLine = 656;
                        CHAN2_MUTE_FB__DOLLAR__  .Value = (ushort) ( Byte( PARAMDATA , (int)( 14 ) ) ) ; 
                        }
                    
                    else 
                        {
                        __context__.SourceCodeLine = 657;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (((((3 + OFFSET) - 1) * 100) + 1) == SV))  ) ) 
                            {
                            __context__.SourceCodeLine = 658;
                            CHAN3_MUTE_FB__DOLLAR__  .Value = (ushort) ( Byte( PARAMDATA , (int)( 14 ) ) ) ; 
                            }
                        
                        else 
                            {
                            __context__.SourceCodeLine = 659;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (((((4 + OFFSET) - 1) * 100) + 1) == SV))  ) ) 
                                {
                                __context__.SourceCodeLine = 660;
                                CHAN4_MUTE_FB__DOLLAR__  .Value = (ushort) ( Byte( PARAMDATA , (int)( 14 ) ) ) ; 
                                }
                            
                            else 
                                {
                                __context__.SourceCodeLine = 661;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (((((5 + OFFSET) - 1) * 100) + 1) == SV))  ) ) 
                                    {
                                    __context__.SourceCodeLine = 662;
                                    CHAN5_MUTE_FB__DOLLAR__  .Value = (ushort) ( Byte( PARAMDATA , (int)( 14 ) ) ) ; 
                                    }
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 663;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (((((6 + OFFSET) - 1) * 100) + 1) == SV))  ) ) 
                                        {
                                        __context__.SourceCodeLine = 664;
                                        CHAN6_MUTE_FB__DOLLAR__  .Value = (ushort) ( Byte( PARAMDATA , (int)( 14 ) ) ) ; 
                                        }
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 665;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (((((7 + OFFSET) - 1) * 100) + 1) == SV))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 666;
                                            CHAN7_MUTE_FB__DOLLAR__  .Value = (ushort) ( Byte( PARAMDATA , (int)( 14 ) ) ) ; 
                                            }
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 667;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (((((8 + OFFSET) - 1) * 100) + 1) == SV))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 668;
                                                CHAN8_MUTE_FB__DOLLAR__  .Value = (ushort) ( Byte( PARAMDATA , (int)( 14 ) ) ) ; 
                                                }
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 669;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (20001 == SV))  ) ) 
                                                    {
                                                    __context__.SourceCodeLine = 670;
                                                    MASTEROUT_MUTE_FB__DOLLAR__  .Value = (ushort) ( Byte( PARAMDATA , (int)( 14 ) ) ) ; 
                                                    }
                                                
                                                }
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 674;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ((((1 + OFFSET) - 1) * 100) == SV))  ) ) 
                    { 
                    __context__.SourceCodeLine = 676;
                    CHAN1_VOL = (ushort) ( VOLUMEPERCENTTOI( __context__ , Functions.Mid( PARAMDATA , (int)( 11 ) , (int)( 4 ) ) ) ) ; 
                    __context__.SourceCodeLine = 677;
                    CHAN1_VOL_FB__DOLLAR__  .Value = (ushort) ( CHAN1_VOL ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 679;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ((((2 + OFFSET) - 1) * 100) == SV))  ) ) 
                        { 
                        __context__.SourceCodeLine = 681;
                        CHAN2_VOL = (ushort) ( VOLUMEPERCENTTOI( __context__ , Functions.Mid( PARAMDATA , (int)( 11 ) , (int)( 4 ) ) ) ) ; 
                        __context__.SourceCodeLine = 682;
                        CHAN2_VOL_FB__DOLLAR__  .Value = (ushort) ( CHAN2_VOL ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 684;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ((((3 + OFFSET) - 1) * 100) == SV))  ) ) 
                            { 
                            __context__.SourceCodeLine = 686;
                            CHAN3_VOL = (ushort) ( VOLUMEPERCENTTOI( __context__ , Functions.Mid( PARAMDATA , (int)( 11 ) , (int)( 4 ) ) ) ) ; 
                            __context__.SourceCodeLine = 687;
                            CHAN3_VOL_FB__DOLLAR__  .Value = (ushort) ( CHAN3_VOL ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 689;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ((((4 + OFFSET) - 1) * 100) == SV))  ) ) 
                                { 
                                __context__.SourceCodeLine = 691;
                                CHAN4_VOL = (ushort) ( VOLUMEPERCENTTOI( __context__ , Functions.Mid( PARAMDATA , (int)( 11 ) , (int)( 4 ) ) ) ) ; 
                                __context__.SourceCodeLine = 692;
                                CHAN4_VOL_FB__DOLLAR__  .Value = (ushort) ( CHAN4_VOL ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 694;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ((((5 + OFFSET) - 1) * 100) == SV))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 696;
                                    CHAN5_VOL = (ushort) ( VOLUMEPERCENTTOI( __context__ , Functions.Mid( PARAMDATA , (int)( 11 ) , (int)( 4 ) ) ) ) ; 
                                    __context__.SourceCodeLine = 697;
                                    CHAN5_VOL_FB__DOLLAR__  .Value = (ushort) ( CHAN5_VOL ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 699;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ((((6 + OFFSET) - 1) * 100) == SV))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 701;
                                        CHAN6_VOL = (ushort) ( VOLUMEPERCENTTOI( __context__ , Functions.Mid( PARAMDATA , (int)( 11 ) , (int)( 4 ) ) ) ) ; 
                                        __context__.SourceCodeLine = 702;
                                        CHAN6_VOL_FB__DOLLAR__  .Value = (ushort) ( CHAN6_VOL ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 704;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ((((7 + OFFSET) - 1) * 100) == SV))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 706;
                                            CHAN7_VOL = (ushort) ( VOLUMEPERCENTTOI( __context__ , Functions.Mid( PARAMDATA , (int)( 11 ) , (int)( 4 ) ) ) ) ; 
                                            __context__.SourceCodeLine = 707;
                                            CHAN7_VOL_FB__DOLLAR__  .Value = (ushort) ( CHAN7_VOL ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 709;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ((((8 + OFFSET) - 1) * 100) == SV))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 711;
                                                CHAN8_VOL = (ushort) ( VOLUMEPERCENTTOI( __context__ , Functions.Mid( PARAMDATA , (int)( 11 ) , (int)( 4 ) ) ) ) ; 
                                                __context__.SourceCodeLine = 712;
                                                CHAN8_VOL_FB__DOLLAR__  .Value = (ushort) ( CHAN8_VOL ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 714;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (20000 == SV))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 716;
                                                    MASTERVOL = (ushort) ( VOLUMEPERCENTTOI( __context__ , Functions.Mid( PARAMDATA , (int)( 11 ) , (int)( 4 ) ) ) ) ; 
                                                    __context__.SourceCodeLine = 717;
                                                    MASTERVOL_FB__DOLLAR__  .Value = (ushort) ( MASTERVOL ) ; 
                                                    } 
                                                
                                                }
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                } 
            
            } 
        
        }
    
    
    }
    
object RX__DOLLAR___OnChange_30 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 726;
        while ( Functions.TestForTrue  ( ( 1)  ) ) 
            { 
            __context__.SourceCodeLine = 728;
            try 
                { 
                __context__.SourceCodeLine = 730;
                TEMPSTRING  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 731;
                TEMPSTRING  .UpdateValue ( Functions.Gather ( "\u0003\u0003\u0003\u0003\u0003" , RX__DOLLAR__ )  ) ; 
                __context__.SourceCodeLine = 732;
                TEMPSTRING  .UpdateValue ( Functions.Left ( TEMPSTRING ,  (int) ( (Functions.Length( TEMPSTRING ) - 4) ) )  ) ; 
                __context__.SourceCodeLine = 733;
                PROCESSRXMESSAGE (  __context__ , TEMPSTRING) ; 
                } 
            
            catch (Exception __splus_exception__)
                { 
                SimplPlusException __splus_exceptionobj__ = new SimplPlusException(__splus_exception__, this );
                
                __context__.SourceCodeLine = 737;
                GenerateUserNotice ( "BSS SoundWeb London Mixer 8-Channel Basic Error: {0}", Functions.GetExceptionMessage (  __splus_exceptionobj__ ) ) ; 
                
                }
                
                __context__.SourceCodeLine = 726;
                } 
            
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 766;
        OFFSET = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 767;
        XOKFEEDBACK = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 768;
        XOKMASTERVOL = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 769;
        XOKCHAN1_VOL = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 770;
        XOKCHAN2_VOL = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 771;
        XOKCHAN3_VOL = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 772;
        XOKCHAN4_VOL = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 773;
        XOKCHAN5_VOL = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 774;
        XOKCHAN6_VOL = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 775;
        XOKCHAN7_VOL = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 776;
        XOKCHAN8_VOL = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 777;
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    RETURNSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 4, this );
    TEMPSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 40, this );
    STEMPSEND  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    SOBJECTIDPADDED  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 10, this );
    SNODE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 6, this );
    
    ENABLE_FEEDBACK__DOLLAR__ = new Crestron.Logos.SplusObjects.DigitalInput( ENABLE_FEEDBACK__DOLLAR____DigitalInput__, this );
    m_DigitalInputList.Add( ENABLE_FEEDBACK__DOLLAR____DigitalInput__, ENABLE_FEEDBACK__DOLLAR__ );
    
    DISABLE_FEEDBACK__DOLLAR__ = new Crestron.Logos.SplusObjects.DigitalInput( DISABLE_FEEDBACK__DOLLAR____DigitalInput__, this );
    m_DigitalInputList.Add( DISABLE_FEEDBACK__DOLLAR____DigitalInput__, DISABLE_FEEDBACK__DOLLAR__ );
    
    CHAN1_MUTE__DOLLAR__ = new Crestron.Logos.SplusObjects.DigitalInput( CHAN1_MUTE__DOLLAR____DigitalInput__, this );
    m_DigitalInputList.Add( CHAN1_MUTE__DOLLAR____DigitalInput__, CHAN1_MUTE__DOLLAR__ );
    
    CHAN2_MUTE__DOLLAR__ = new Crestron.Logos.SplusObjects.DigitalInput( CHAN2_MUTE__DOLLAR____DigitalInput__, this );
    m_DigitalInputList.Add( CHAN2_MUTE__DOLLAR____DigitalInput__, CHAN2_MUTE__DOLLAR__ );
    
    CHAN3_MUTE__DOLLAR__ = new Crestron.Logos.SplusObjects.DigitalInput( CHAN3_MUTE__DOLLAR____DigitalInput__, this );
    m_DigitalInputList.Add( CHAN3_MUTE__DOLLAR____DigitalInput__, CHAN3_MUTE__DOLLAR__ );
    
    CHAN4_MUTE__DOLLAR__ = new Crestron.Logos.SplusObjects.DigitalInput( CHAN4_MUTE__DOLLAR____DigitalInput__, this );
    m_DigitalInputList.Add( CHAN4_MUTE__DOLLAR____DigitalInput__, CHAN4_MUTE__DOLLAR__ );
    
    CHAN5_MUTE__DOLLAR__ = new Crestron.Logos.SplusObjects.DigitalInput( CHAN5_MUTE__DOLLAR____DigitalInput__, this );
    m_DigitalInputList.Add( CHAN5_MUTE__DOLLAR____DigitalInput__, CHAN5_MUTE__DOLLAR__ );
    
    CHAN6_MUTE__DOLLAR__ = new Crestron.Logos.SplusObjects.DigitalInput( CHAN6_MUTE__DOLLAR____DigitalInput__, this );
    m_DigitalInputList.Add( CHAN6_MUTE__DOLLAR____DigitalInput__, CHAN6_MUTE__DOLLAR__ );
    
    CHAN7_MUTE__DOLLAR__ = new Crestron.Logos.SplusObjects.DigitalInput( CHAN7_MUTE__DOLLAR____DigitalInput__, this );
    m_DigitalInputList.Add( CHAN7_MUTE__DOLLAR____DigitalInput__, CHAN7_MUTE__DOLLAR__ );
    
    CHAN8_MUTE__DOLLAR__ = new Crestron.Logos.SplusObjects.DigitalInput( CHAN8_MUTE__DOLLAR____DigitalInput__, this );
    m_DigitalInputList.Add( CHAN8_MUTE__DOLLAR____DigitalInput__, CHAN8_MUTE__DOLLAR__ );
    
    MASTEROUT_MUTE__DOLLAR__ = new Crestron.Logos.SplusObjects.DigitalInput( MASTEROUT_MUTE__DOLLAR____DigitalInput__, this );
    m_DigitalInputList.Add( MASTEROUT_MUTE__DOLLAR____DigitalInput__, MASTEROUT_MUTE__DOLLAR__ );
    
    CHAN1_MUTE_FB__DOLLAR__ = new Crestron.Logos.SplusObjects.DigitalOutput( CHAN1_MUTE_FB__DOLLAR____DigitalOutput__, this );
    m_DigitalOutputList.Add( CHAN1_MUTE_FB__DOLLAR____DigitalOutput__, CHAN1_MUTE_FB__DOLLAR__ );
    
    CHAN2_MUTE_FB__DOLLAR__ = new Crestron.Logos.SplusObjects.DigitalOutput( CHAN2_MUTE_FB__DOLLAR____DigitalOutput__, this );
    m_DigitalOutputList.Add( CHAN2_MUTE_FB__DOLLAR____DigitalOutput__, CHAN2_MUTE_FB__DOLLAR__ );
    
    CHAN3_MUTE_FB__DOLLAR__ = new Crestron.Logos.SplusObjects.DigitalOutput( CHAN3_MUTE_FB__DOLLAR____DigitalOutput__, this );
    m_DigitalOutputList.Add( CHAN3_MUTE_FB__DOLLAR____DigitalOutput__, CHAN3_MUTE_FB__DOLLAR__ );
    
    CHAN4_MUTE_FB__DOLLAR__ = new Crestron.Logos.SplusObjects.DigitalOutput( CHAN4_MUTE_FB__DOLLAR____DigitalOutput__, this );
    m_DigitalOutputList.Add( CHAN4_MUTE_FB__DOLLAR____DigitalOutput__, CHAN4_MUTE_FB__DOLLAR__ );
    
    CHAN5_MUTE_FB__DOLLAR__ = new Crestron.Logos.SplusObjects.DigitalOutput( CHAN5_MUTE_FB__DOLLAR____DigitalOutput__, this );
    m_DigitalOutputList.Add( CHAN5_MUTE_FB__DOLLAR____DigitalOutput__, CHAN5_MUTE_FB__DOLLAR__ );
    
    CHAN6_MUTE_FB__DOLLAR__ = new Crestron.Logos.SplusObjects.DigitalOutput( CHAN6_MUTE_FB__DOLLAR____DigitalOutput__, this );
    m_DigitalOutputList.Add( CHAN6_MUTE_FB__DOLLAR____DigitalOutput__, CHAN6_MUTE_FB__DOLLAR__ );
    
    CHAN7_MUTE_FB__DOLLAR__ = new Crestron.Logos.SplusObjects.DigitalOutput( CHAN7_MUTE_FB__DOLLAR____DigitalOutput__, this );
    m_DigitalOutputList.Add( CHAN7_MUTE_FB__DOLLAR____DigitalOutput__, CHAN7_MUTE_FB__DOLLAR__ );
    
    CHAN8_MUTE_FB__DOLLAR__ = new Crestron.Logos.SplusObjects.DigitalOutput( CHAN8_MUTE_FB__DOLLAR____DigitalOutput__, this );
    m_DigitalOutputList.Add( CHAN8_MUTE_FB__DOLLAR____DigitalOutput__, CHAN8_MUTE_FB__DOLLAR__ );
    
    MASTEROUT_MUTE_FB__DOLLAR__ = new Crestron.Logos.SplusObjects.DigitalOutput( MASTEROUT_MUTE_FB__DOLLAR____DigitalOutput__, this );
    m_DigitalOutputList.Add( MASTEROUT_MUTE_FB__DOLLAR____DigitalOutput__, MASTEROUT_MUTE_FB__DOLLAR__ );
    
    CHAN_OFFSET__DOLLAR__ = new Crestron.Logos.SplusObjects.AnalogInput( CHAN_OFFSET__DOLLAR____AnalogSerialInput__, this );
    m_AnalogInputList.Add( CHAN_OFFSET__DOLLAR____AnalogSerialInput__, CHAN_OFFSET__DOLLAR__ );
    
    CHAN1_VOL__DOLLAR__ = new Crestron.Logos.SplusObjects.AnalogInput( CHAN1_VOL__DOLLAR____AnalogSerialInput__, this );
    m_AnalogInputList.Add( CHAN1_VOL__DOLLAR____AnalogSerialInput__, CHAN1_VOL__DOLLAR__ );
    
    CHAN2_VOL__DOLLAR__ = new Crestron.Logos.SplusObjects.AnalogInput( CHAN2_VOL__DOLLAR____AnalogSerialInput__, this );
    m_AnalogInputList.Add( CHAN2_VOL__DOLLAR____AnalogSerialInput__, CHAN2_VOL__DOLLAR__ );
    
    CHAN3_VOL__DOLLAR__ = new Crestron.Logos.SplusObjects.AnalogInput( CHAN3_VOL__DOLLAR____AnalogSerialInput__, this );
    m_AnalogInputList.Add( CHAN3_VOL__DOLLAR____AnalogSerialInput__, CHAN3_VOL__DOLLAR__ );
    
    CHAN4_VOL__DOLLAR__ = new Crestron.Logos.SplusObjects.AnalogInput( CHAN4_VOL__DOLLAR____AnalogSerialInput__, this );
    m_AnalogInputList.Add( CHAN4_VOL__DOLLAR____AnalogSerialInput__, CHAN4_VOL__DOLLAR__ );
    
    CHAN5_VOL__DOLLAR__ = new Crestron.Logos.SplusObjects.AnalogInput( CHAN5_VOL__DOLLAR____AnalogSerialInput__, this );
    m_AnalogInputList.Add( CHAN5_VOL__DOLLAR____AnalogSerialInput__, CHAN5_VOL__DOLLAR__ );
    
    CHAN6_VOL__DOLLAR__ = new Crestron.Logos.SplusObjects.AnalogInput( CHAN6_VOL__DOLLAR____AnalogSerialInput__, this );
    m_AnalogInputList.Add( CHAN6_VOL__DOLLAR____AnalogSerialInput__, CHAN6_VOL__DOLLAR__ );
    
    CHAN7_VOL__DOLLAR__ = new Crestron.Logos.SplusObjects.AnalogInput( CHAN7_VOL__DOLLAR____AnalogSerialInput__, this );
    m_AnalogInputList.Add( CHAN7_VOL__DOLLAR____AnalogSerialInput__, CHAN7_VOL__DOLLAR__ );
    
    CHAN8_VOL__DOLLAR__ = new Crestron.Logos.SplusObjects.AnalogInput( CHAN8_VOL__DOLLAR____AnalogSerialInput__, this );
    m_AnalogInputList.Add( CHAN8_VOL__DOLLAR____AnalogSerialInput__, CHAN8_VOL__DOLLAR__ );
    
    MASTERVOL__DOLLAR__ = new Crestron.Logos.SplusObjects.AnalogInput( MASTERVOL__DOLLAR____AnalogSerialInput__, this );
    m_AnalogInputList.Add( MASTERVOL__DOLLAR____AnalogSerialInput__, MASTERVOL__DOLLAR__ );
    
    CHAN1_VOL_FB__DOLLAR__ = new Crestron.Logos.SplusObjects.AnalogOutput( CHAN1_VOL_FB__DOLLAR____AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CHAN1_VOL_FB__DOLLAR____AnalogSerialOutput__, CHAN1_VOL_FB__DOLLAR__ );
    
    CHAN2_VOL_FB__DOLLAR__ = new Crestron.Logos.SplusObjects.AnalogOutput( CHAN2_VOL_FB__DOLLAR____AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CHAN2_VOL_FB__DOLLAR____AnalogSerialOutput__, CHAN2_VOL_FB__DOLLAR__ );
    
    CHAN3_VOL_FB__DOLLAR__ = new Crestron.Logos.SplusObjects.AnalogOutput( CHAN3_VOL_FB__DOLLAR____AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CHAN3_VOL_FB__DOLLAR____AnalogSerialOutput__, CHAN3_VOL_FB__DOLLAR__ );
    
    CHAN4_VOL_FB__DOLLAR__ = new Crestron.Logos.SplusObjects.AnalogOutput( CHAN4_VOL_FB__DOLLAR____AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CHAN4_VOL_FB__DOLLAR____AnalogSerialOutput__, CHAN4_VOL_FB__DOLLAR__ );
    
    CHAN5_VOL_FB__DOLLAR__ = new Crestron.Logos.SplusObjects.AnalogOutput( CHAN5_VOL_FB__DOLLAR____AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CHAN5_VOL_FB__DOLLAR____AnalogSerialOutput__, CHAN5_VOL_FB__DOLLAR__ );
    
    CHAN6_VOL_FB__DOLLAR__ = new Crestron.Logos.SplusObjects.AnalogOutput( CHAN6_VOL_FB__DOLLAR____AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CHAN6_VOL_FB__DOLLAR____AnalogSerialOutput__, CHAN6_VOL_FB__DOLLAR__ );
    
    CHAN7_VOL_FB__DOLLAR__ = new Crestron.Logos.SplusObjects.AnalogOutput( CHAN7_VOL_FB__DOLLAR____AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CHAN7_VOL_FB__DOLLAR____AnalogSerialOutput__, CHAN7_VOL_FB__DOLLAR__ );
    
    CHAN8_VOL_FB__DOLLAR__ = new Crestron.Logos.SplusObjects.AnalogOutput( CHAN8_VOL_FB__DOLLAR____AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CHAN8_VOL_FB__DOLLAR____AnalogSerialOutput__, CHAN8_VOL_FB__DOLLAR__ );
    
    MASTERVOL_FB__DOLLAR__ = new Crestron.Logos.SplusObjects.AnalogOutput( MASTERVOL_FB__DOLLAR____AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( MASTERVOL_FB__DOLLAR____AnalogSerialOutput__, MASTERVOL_FB__DOLLAR__ );
    
    RX__DOLLAR__ = new Crestron.Logos.SplusObjects.StringInput( RX__DOLLAR____AnalogSerialInput__, 4000, this );
    m_StringInputList.Add( RX__DOLLAR____AnalogSerialInput__, RX__DOLLAR__ );
    
    TX__DOLLAR__ = new Crestron.Logos.SplusObjects.StringOutput( TX__DOLLAR____AnalogSerialOutput__, this );
    m_StringOutputList.Add( TX__DOLLAR____AnalogSerialOutput__, TX__DOLLAR__ );
    
    OBJECTID__DOLLAR__ = new StringParameter( OBJECTID__DOLLAR____Parameter__, this );
    m_ParameterList.Add( OBJECTID__DOLLAR____Parameter__, OBJECTID__DOLLAR__ );
    
    __SPLS_TMPVAR__WAITLABEL_7___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_7___CallbackFn );
    
    CHAN_OFFSET__DOLLAR__.OnAnalogChange.Add( new InputChangeHandlerWrapper( CHAN_OFFSET__DOLLAR___OnChange_0, false ) );
    ENABLE_FEEDBACK__DOLLAR__.OnDigitalPush.Add( new InputChangeHandlerWrapper( ENABLE_FEEDBACK__DOLLAR___OnPush_1, false ) );
    DISABLE_FEEDBACK__DOLLAR__.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISABLE_FEEDBACK__DOLLAR___OnPush_2, false ) );
    CHAN1_MUTE__DOLLAR__.OnDigitalPush.Add( new InputChangeHandlerWrapper( CHAN1_MUTE__DOLLAR___OnPush_3, false ) );
    CHAN1_MUTE__DOLLAR__.OnDigitalRelease.Add( new InputChangeHandlerWrapper( CHAN1_MUTE__DOLLAR___OnRelease_4, false ) );
    CHAN2_MUTE__DOLLAR__.OnDigitalPush.Add( new InputChangeHandlerWrapper( CHAN2_MUTE__DOLLAR___OnPush_5, false ) );
    CHAN2_MUTE__DOLLAR__.OnDigitalRelease.Add( new InputChangeHandlerWrapper( CHAN2_MUTE__DOLLAR___OnRelease_6, false ) );
    CHAN3_MUTE__DOLLAR__.OnDigitalPush.Add( new InputChangeHandlerWrapper( CHAN3_MUTE__DOLLAR___OnPush_7, false ) );
    CHAN3_MUTE__DOLLAR__.OnDigitalRelease.Add( new InputChangeHandlerWrapper( CHAN3_MUTE__DOLLAR___OnRelease_8, false ) );
    CHAN4_MUTE__DOLLAR__.OnDigitalPush.Add( new InputChangeHandlerWrapper( CHAN4_MUTE__DOLLAR___OnPush_9, false ) );
    CHAN4_MUTE__DOLLAR__.OnDigitalRelease.Add( new InputChangeHandlerWrapper( CHAN4_MUTE__DOLLAR___OnRelease_10, false ) );
    CHAN5_MUTE__DOLLAR__.OnDigitalPush.Add( new InputChangeHandlerWrapper( CHAN5_MUTE__DOLLAR___OnPush_11, false ) );
    CHAN5_MUTE__DOLLAR__.OnDigitalRelease.Add( new InputChangeHandlerWrapper( CHAN5_MUTE__DOLLAR___OnRelease_12, false ) );
    CHAN6_MUTE__DOLLAR__.OnDigitalPush.Add( new InputChangeHandlerWrapper( CHAN6_MUTE__DOLLAR___OnPush_13, false ) );
    CHAN6_MUTE__DOLLAR__.OnDigitalRelease.Add( new InputChangeHandlerWrapper( CHAN6_MUTE__DOLLAR___OnRelease_14, false ) );
    CHAN7_MUTE__DOLLAR__.OnDigitalPush.Add( new InputChangeHandlerWrapper( CHAN7_MUTE__DOLLAR___OnPush_15, false ) );
    CHAN7_MUTE__DOLLAR__.OnDigitalRelease.Add( new InputChangeHandlerWrapper( CHAN7_MUTE__DOLLAR___OnRelease_16, false ) );
    CHAN8_MUTE__DOLLAR__.OnDigitalPush.Add( new InputChangeHandlerWrapper( CHAN8_MUTE__DOLLAR___OnPush_17, false ) );
    CHAN8_MUTE__DOLLAR__.OnDigitalRelease.Add( new InputChangeHandlerWrapper( CHAN8_MUTE__DOLLAR___OnRelease_18, false ) );
    MASTEROUT_MUTE__DOLLAR__.OnDigitalPush.Add( new InputChangeHandlerWrapper( MASTEROUT_MUTE__DOLLAR___OnPush_19, false ) );
    MASTEROUT_MUTE__DOLLAR__.OnDigitalRelease.Add( new InputChangeHandlerWrapper( MASTEROUT_MUTE__DOLLAR___OnRelease_20, false ) );
    CHAN1_VOL__DOLLAR__.OnAnalogChange.Add( new InputChangeHandlerWrapper( CHAN1_VOL__DOLLAR___OnChange_21, false ) );
    CHAN2_VOL__DOLLAR__.OnAnalogChange.Add( new InputChangeHandlerWrapper( CHAN2_VOL__DOLLAR___OnChange_22, false ) );
    CHAN3_VOL__DOLLAR__.OnAnalogChange.Add( new InputChangeHandlerWrapper( CHAN3_VOL__DOLLAR___OnChange_23, false ) );
    CHAN4_VOL__DOLLAR__.OnAnalogChange.Add( new InputChangeHandlerWrapper( CHAN4_VOL__DOLLAR___OnChange_24, false ) );
    CHAN5_VOL__DOLLAR__.OnAnalogChange.Add( new InputChangeHandlerWrapper( CHAN5_VOL__DOLLAR___OnChange_25, false ) );
    CHAN6_VOL__DOLLAR__.OnAnalogChange.Add( new InputChangeHandlerWrapper( CHAN6_VOL__DOLLAR___OnChange_26, false ) );
    CHAN7_VOL__DOLLAR__.OnAnalogChange.Add( new InputChangeHandlerWrapper( CHAN7_VOL__DOLLAR___OnChange_27, false ) );
    CHAN8_VOL__DOLLAR__.OnAnalogChange.Add( new InputChangeHandlerWrapper( CHAN8_VOL__DOLLAR___OnChange_28, false ) );
    MASTERVOL__DOLLAR__.OnAnalogChange.Add( new InputChangeHandlerWrapper( MASTERVOL__DOLLAR___OnChange_29, false ) );
    RX__DOLLAR__.OnSerialChange.Add( new InputChangeHandlerWrapper( RX__DOLLAR___OnChange_30, true ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_BSS_SOUNDWEB_LONDON_MIXER_8_CHANNEL_BASIC_V5_4 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction __SPLS_TMPVAR__WAITLABEL_7___Callback;


const uint ENABLE_FEEDBACK__DOLLAR____DigitalInput__ = 0;
const uint DISABLE_FEEDBACK__DOLLAR____DigitalInput__ = 1;
const uint CHAN1_MUTE__DOLLAR____DigitalInput__ = 2;
const uint CHAN2_MUTE__DOLLAR____DigitalInput__ = 3;
const uint CHAN3_MUTE__DOLLAR____DigitalInput__ = 4;
const uint CHAN4_MUTE__DOLLAR____DigitalInput__ = 5;
const uint CHAN5_MUTE__DOLLAR____DigitalInput__ = 6;
const uint CHAN6_MUTE__DOLLAR____DigitalInput__ = 7;
const uint CHAN7_MUTE__DOLLAR____DigitalInput__ = 8;
const uint CHAN8_MUTE__DOLLAR____DigitalInput__ = 9;
const uint MASTEROUT_MUTE__DOLLAR____DigitalInput__ = 10;
const uint CHAN_OFFSET__DOLLAR____AnalogSerialInput__ = 0;
const uint CHAN1_VOL__DOLLAR____AnalogSerialInput__ = 1;
const uint CHAN2_VOL__DOLLAR____AnalogSerialInput__ = 2;
const uint CHAN3_VOL__DOLLAR____AnalogSerialInput__ = 3;
const uint CHAN4_VOL__DOLLAR____AnalogSerialInput__ = 4;
const uint CHAN5_VOL__DOLLAR____AnalogSerialInput__ = 5;
const uint CHAN6_VOL__DOLLAR____AnalogSerialInput__ = 6;
const uint CHAN7_VOL__DOLLAR____AnalogSerialInput__ = 7;
const uint CHAN8_VOL__DOLLAR____AnalogSerialInput__ = 8;
const uint MASTERVOL__DOLLAR____AnalogSerialInput__ = 9;
const uint RX__DOLLAR____AnalogSerialInput__ = 10;
const uint CHAN1_MUTE_FB__DOLLAR____DigitalOutput__ = 0;
const uint CHAN2_MUTE_FB__DOLLAR____DigitalOutput__ = 1;
const uint CHAN3_MUTE_FB__DOLLAR____DigitalOutput__ = 2;
const uint CHAN4_MUTE_FB__DOLLAR____DigitalOutput__ = 3;
const uint CHAN5_MUTE_FB__DOLLAR____DigitalOutput__ = 4;
const uint CHAN6_MUTE_FB__DOLLAR____DigitalOutput__ = 5;
const uint CHAN7_MUTE_FB__DOLLAR____DigitalOutput__ = 6;
const uint CHAN8_MUTE_FB__DOLLAR____DigitalOutput__ = 7;
const uint MASTEROUT_MUTE_FB__DOLLAR____DigitalOutput__ = 8;
const uint CHAN1_VOL_FB__DOLLAR____AnalogSerialOutput__ = 0;
const uint CHAN2_VOL_FB__DOLLAR____AnalogSerialOutput__ = 1;
const uint CHAN3_VOL_FB__DOLLAR____AnalogSerialOutput__ = 2;
const uint CHAN4_VOL_FB__DOLLAR____AnalogSerialOutput__ = 3;
const uint CHAN5_VOL_FB__DOLLAR____AnalogSerialOutput__ = 4;
const uint CHAN6_VOL_FB__DOLLAR____AnalogSerialOutput__ = 5;
const uint CHAN7_VOL_FB__DOLLAR____AnalogSerialOutput__ = 6;
const uint CHAN8_VOL_FB__DOLLAR____AnalogSerialOutput__ = 7;
const uint MASTERVOL_FB__DOLLAR____AnalogSerialOutput__ = 8;
const uint TX__DOLLAR____AnalogSerialOutput__ = 9;
const uint OBJECTID__DOLLAR____Parameter__ = 10;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
