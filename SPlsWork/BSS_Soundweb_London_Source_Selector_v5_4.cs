using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_BSS_SOUNDWEB_LONDON_SOURCE_SELECTOR_V5_4
{
    public class UserModuleClass_BSS_SOUNDWEB_LONDON_SOURCE_SELECTOR_V5_4 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput SUBSCRIBE__DOLLAR__;
        Crestron.Logos.SplusObjects.DigitalInput UNSUBSCRIBE__DOLLAR__;
        Crestron.Logos.SplusObjects.AnalogInput INPUT__DOLLAR__;
        Crestron.Logos.SplusObjects.BufferInput RX__DOLLAR__;
        Crestron.Logos.SplusObjects.AnalogOutput INPUT_FB__DOLLAR__;
        Crestron.Logos.SplusObjects.StringOutput TX__DOLLAR__;
        StringParameter OBJECTID__DOLLAR__;
        ushort XOKSUBSCRIBE = 0;
        ushort I = 0;
        ushort ITEMPNUM = 0;
        ushort IMARKER1 = 0;
        CrestronString TEMPSTRING;
        CrestronString STEMPSEND;
        CrestronString SNODE;
        CrestronString SOBJECTIDPADDED;
        ushort X = 0;
        ushort STATEVAR = 0;
        ushort SUBSCRIBE = 0;
        private CrestronString SEND (  SplusExecutionContext __context__, CrestronString STR1 ) 
            { 
            ushort CHECKSUM = 0;
            
            CrestronString FSTEMPRETURN;
            CrestronString SENDSTRING;
            FSTEMPRETURN  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
            SENDSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
            
            
            __context__.SourceCodeLine = 79;
            CHECKSUM = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 80;
            SENDSTRING  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 81;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)Functions.Length( STR1 ); 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 83;
                CHECKSUM = (ushort) ( (CHECKSUM ^ Byte( STR1 , (int)( I ) )) ) ; 
                __context__.SourceCodeLine = 84;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (Byte( STR1 , (int)( I ) ) == 2) ) || Functions.TestForTrue ( Functions.BoolToInt (Byte( STR1 , (int)( I ) ) == 3) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (Byte( STR1 , (int)( I ) ) == 6) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (Byte( STR1 , (int)( I ) ) == 21) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (Byte( STR1 , (int)( I ) ) == 27) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 86;
                    MakeString ( SENDSTRING , "{0}\u001B{1}", SENDSTRING , Functions.Chr (  (int) ( (Byte( STR1 , (int)( I ) ) + 128) ) ) ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 90;
                    MakeString ( SENDSTRING , "{0}{1}", SENDSTRING , Functions.Chr (  (int) ( Byte( STR1 , (int)( I ) ) ) ) ) ; 
                    } 
                
                __context__.SourceCodeLine = 81;
                } 
            
            __context__.SourceCodeLine = 93;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (CHECKSUM == 2) ) || Functions.TestForTrue ( Functions.BoolToInt (CHECKSUM == 3) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (CHECKSUM == 6) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (CHECKSUM == 21) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (CHECKSUM == 27) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 95;
                MakeString ( FSTEMPRETURN , "\u0002{0}\u001B{1}\u0003", SENDSTRING , Functions.Chr (  (int) ( (CHECKSUM + 128) ) ) ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 99;
                MakeString ( FSTEMPRETURN , "\u0002{0}{1}\u0003", SENDSTRING , Functions.Chr (  (int) ( CHECKSUM ) ) ) ; 
                } 
            
            __context__.SourceCodeLine = 101;
            return ( FSTEMPRETURN ) ; 
            
            }
            
        private CrestronString FPAD (  SplusExecutionContext __context__, CrestronString FSPAD ) 
            { 
            ushort FIA = 0;
            
            CrestronString FSRETURN;
            FSRETURN  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 10, this );
            
            
            __context__.SourceCodeLine = 109;
            FSRETURN  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 110;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)Functions.Length( FSPAD ); 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( FIA  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (FIA  >= __FN_FORSTART_VAL__1) && (FIA  <= __FN_FOREND_VAL__1) ) : ( (FIA  <= __FN_FORSTART_VAL__1) && (FIA  >= __FN_FOREND_VAL__1) ) ; FIA  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 112;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (Byte( FSPAD , (int)( FIA ) ) == 2) ) || Functions.TestForTrue ( Functions.BoolToInt (Byte( FSPAD , (int)( FIA ) ) == 3) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (Byte( FSPAD , (int)( FIA ) ) == 6) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (Byte( FSPAD , (int)( FIA ) ) == 21) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (Byte( FSPAD , (int)( FIA ) ) == 27) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 114;
                    MakeString ( FSRETURN , "{0}\u001B{1}", FSRETURN , Functions.Chr (  (int) ( (Byte( FSPAD , (int)( FIA ) ) + 128) ) ) ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 118;
                    MakeString ( FSRETURN , "{0}{1}", FSRETURN , Functions.Chr (  (int) ( Byte( FSPAD , (int)( FIA ) ) ) ) ) ; 
                    } 
                
                __context__.SourceCodeLine = 110;
                } 
            
            __context__.SourceCodeLine = 121;
            return ( FSRETURN ) ; 
            
            }
            
        private CrestronString FUNPAD (  SplusExecutionContext __context__, CrestronString STR2 ) 
            { 
            ushort J = 0;
            
            CrestronString RECEIVESTRING;
            RECEIVESTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
            
            
            __context__.SourceCodeLine = 129;
            RECEIVESTRING  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 130;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)Functions.Length( STR2 ); 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( J  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (J  >= __FN_FORSTART_VAL__1) && (J  <= __FN_FOREND_VAL__1) ) : ( (J  <= __FN_FORSTART_VAL__1) && (J  >= __FN_FOREND_VAL__1) ) ; J  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 132;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Byte( STR2 , (int)( J ) ) == 27))  ) ) 
                    { 
                    __context__.SourceCodeLine = 134;
                    RECEIVESTRING  .UpdateValue ( RECEIVESTRING + Functions.Chr (  (int) ( (Byte( STR2 , (int)( (J + 1) ) ) - 128) ) )  ) ; 
                    __context__.SourceCodeLine = 135;
                    J = (ushort) ( (J + 1) ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 139;
                    RECEIVESTRING  .UpdateValue ( RECEIVESTRING + Functions.Chr (  (int) ( Byte( STR2 , (int)( J ) ) ) )  ) ; 
                    } 
                
                __context__.SourceCodeLine = 130;
                } 
            
            __context__.SourceCodeLine = 142;
            while ( Functions.TestForTrue  ( ( Functions.BoolToInt (Byte( RECEIVESTRING , (int)( 1 ) ) == 6))  ) ) 
                { 
                __context__.SourceCodeLine = 144;
                RECEIVESTRING  .UpdateValue ( Functions.Right ( RECEIVESTRING ,  (int) ( (Functions.Length( RECEIVESTRING ) - 1) ) )  ) ; 
                __context__.SourceCodeLine = 142;
                } 
            
            __context__.SourceCodeLine = 146;
            return ( RECEIVESTRING ) ; 
            
            }
            
        object INPUT__DOLLAR___OnChange_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 154;
                MakeString ( STEMPSEND , "\u0088{0}\u0003{1}\u0000\u0000\u0000\u0000\u0000{2}", SNODE , OBJECTID__DOLLAR__ , Functions.Chr (  (int) ( INPUT__DOLLAR__  .UshortValue ) ) ) ; 
                __context__.SourceCodeLine = 155;
                TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
                __context__.SourceCodeLine = 157;
                if ( Functions.TestForTrue  ( ( SUBSCRIBE__DOLLAR__  .Value)  ) ) 
                    { 
                    __context__.SourceCodeLine = 159;
                    MakeString ( STEMPSEND , "\u0089{0}\u0003{1}\u0000\u0000\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ ) ; 
                    __context__.SourceCodeLine = 160;
                    TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
                    } 
                
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object SUBSCRIBE__DOLLAR___OnPush_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 166;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_6__" , 20 , __SPLS_TMPVAR__WAITLABEL_6___Callback ) ;
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
public void __SPLS_TMPVAR__WAITLABEL_6___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 168;
            if ( Functions.TestForTrue  ( ( XOKSUBSCRIBE)  ) ) 
                { 
                __context__.SourceCodeLine = 170;
                XOKSUBSCRIBE = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 171;
                MakeString ( STEMPSEND , "\u0089{0}\u0003{1}\u0000\u0000\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ ) ; 
                __context__.SourceCodeLine = 172;
                TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
                __context__.SourceCodeLine = 173;
                XOKSUBSCRIBE = (ushort) ( 1 ) ; 
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object UNSUBSCRIBE__DOLLAR___OnPush_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 180;
        if ( Functions.TestForTrue  ( ( XOKSUBSCRIBE)  ) ) 
            { 
            __context__.SourceCodeLine = 182;
            XOKSUBSCRIBE = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 183;
            MakeString ( STEMPSEND , "\u008A{0}\u0003{1}\u0000\u0000\u0000\u0000\u0000\u0000", SNODE , OBJECTID__DOLLAR__ ) ; 
            __context__.SourceCodeLine = 184;
            TX__DOLLAR__  .UpdateValue ( SEND (  __context__ , STEMPSEND) + "\u0003\u0003\u0003\u0003"  ) ; 
            __context__.SourceCodeLine = 185;
            XOKSUBSCRIBE = (ushort) ( 1 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

private void PROCESSRXMESSAGE (  SplusExecutionContext __context__, CrestronString PARAMDATA ) 
    { 
    
    __context__.SourceCodeLine = 191;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Find( "Send Name" , PARAMDATA ) > 0 ))  ) ) 
        { 
        __context__.SourceCodeLine = 193;
        ITEMPNUM = (ushort) ( Functions.Atoi( PARAMDATA ) ) ; 
        __context__.SourceCodeLine = 194;
        IMARKER1 = (ushort) ( (Functions.Find( "Node " , PARAMDATA ) + 5) ) ; 
        __context__.SourceCodeLine = 195;
        SNODE  .UpdateValue ( Functions.Mid ( PARAMDATA ,  (int) ( IMARKER1 ) ,  (int) ( 2 ) )  ) ; 
        __context__.SourceCodeLine = 196;
        SOBJECTIDPADDED  .UpdateValue ( FPAD (  __context__ , OBJECTID__DOLLAR__ )  ) ; 
        __context__.SourceCodeLine = 197;
        MakeString ( TX__DOLLAR__ , "Send Name{0:d} = {1}\u0003\u0003\u0003\u0003\u0003", (short)ITEMPNUM, SOBJECTIDPADDED ) ; 
        } 
    
    else 
        {
        __context__.SourceCodeLine = 199;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (Functions.Mid( PARAMDATA , (int)( 7 ) , (int)( 3 ) ) == "\u0000\u0000\u0000") ) || Functions.TestForTrue ( Functions.BoolToInt (Functions.Mid( PARAMDATA , (int)( 7 ) , (int)( Functions.Length( SOBJECTIDPADDED ) ) ) == SOBJECTIDPADDED) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 201;
            PARAMDATA  .UpdateValue ( FUNPAD (  __context__ , PARAMDATA)  ) ; 
            __context__.SourceCodeLine = 202;
            INPUT_FB__DOLLAR__  .Value = (ushort) ( Byte( PARAMDATA , (int)( 14 ) ) ) ; 
            } 
        
        }
    
    
    }
    
object RX__DOLLAR___OnChange_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 209;
        while ( Functions.TestForTrue  ( ( 1)  ) ) 
            { 
            __context__.SourceCodeLine = 211;
            try 
                { 
                __context__.SourceCodeLine = 213;
                TEMPSTRING  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 214;
                TEMPSTRING  .UpdateValue ( Functions.Gather ( "\u0003\u0003\u0003\u0003\u0003" , RX__DOLLAR__ )  ) ; 
                __context__.SourceCodeLine = 215;
                TEMPSTRING  .UpdateValue ( Functions.Left ( TEMPSTRING ,  (int) ( (Functions.Length( TEMPSTRING ) - 4) ) )  ) ; 
                __context__.SourceCodeLine = 216;
                PROCESSRXMESSAGE (  __context__ , TEMPSTRING) ; 
                } 
            
            catch (Exception __splus_exception__)
                { 
                SimplPlusException __splus_exceptionobj__ = new SimplPlusException(__splus_exception__, this );
                
                __context__.SourceCodeLine = 220;
                GenerateUserNotice ( "BSS SoundWeb London Source Selector Error: {0}", Functions.GetExceptionMessage (  __splus_exceptionobj__ ) ) ; 
                
                }
                
                __context__.SourceCodeLine = 209;
                } 
            
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 249;
        SUBSCRIBE = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 250;
        XOKSUBSCRIBE = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 251;
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    TEMPSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 40, this );
    STEMPSEND  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    SNODE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 6, this );
    SOBJECTIDPADDED  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 120, this );
    
    SUBSCRIBE__DOLLAR__ = new Crestron.Logos.SplusObjects.DigitalInput( SUBSCRIBE__DOLLAR____DigitalInput__, this );
    m_DigitalInputList.Add( SUBSCRIBE__DOLLAR____DigitalInput__, SUBSCRIBE__DOLLAR__ );
    
    UNSUBSCRIBE__DOLLAR__ = new Crestron.Logos.SplusObjects.DigitalInput( UNSUBSCRIBE__DOLLAR____DigitalInput__, this );
    m_DigitalInputList.Add( UNSUBSCRIBE__DOLLAR____DigitalInput__, UNSUBSCRIBE__DOLLAR__ );
    
    INPUT__DOLLAR__ = new Crestron.Logos.SplusObjects.AnalogInput( INPUT__DOLLAR____AnalogSerialInput__, this );
    m_AnalogInputList.Add( INPUT__DOLLAR____AnalogSerialInput__, INPUT__DOLLAR__ );
    
    INPUT_FB__DOLLAR__ = new Crestron.Logos.SplusObjects.AnalogOutput( INPUT_FB__DOLLAR____AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( INPUT_FB__DOLLAR____AnalogSerialOutput__, INPUT_FB__DOLLAR__ );
    
    TX__DOLLAR__ = new Crestron.Logos.SplusObjects.StringOutput( TX__DOLLAR____AnalogSerialOutput__, this );
    m_StringOutputList.Add( TX__DOLLAR____AnalogSerialOutput__, TX__DOLLAR__ );
    
    RX__DOLLAR__ = new Crestron.Logos.SplusObjects.BufferInput( RX__DOLLAR____AnalogSerialInput__, 4000, this );
    m_StringInputList.Add( RX__DOLLAR____AnalogSerialInput__, RX__DOLLAR__ );
    
    OBJECTID__DOLLAR__ = new StringParameter( OBJECTID__DOLLAR____Parameter__, this );
    m_ParameterList.Add( OBJECTID__DOLLAR____Parameter__, OBJECTID__DOLLAR__ );
    
    __SPLS_TMPVAR__WAITLABEL_6___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_6___CallbackFn );
    
    INPUT__DOLLAR__.OnAnalogChange.Add( new InputChangeHandlerWrapper( INPUT__DOLLAR___OnChange_0, false ) );
    SUBSCRIBE__DOLLAR__.OnDigitalPush.Add( new InputChangeHandlerWrapper( SUBSCRIBE__DOLLAR___OnPush_1, false ) );
    UNSUBSCRIBE__DOLLAR__.OnDigitalPush.Add( new InputChangeHandlerWrapper( UNSUBSCRIBE__DOLLAR___OnPush_2, false ) );
    RX__DOLLAR__.OnSerialChange.Add( new InputChangeHandlerWrapper( RX__DOLLAR___OnChange_3, true ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_BSS_SOUNDWEB_LONDON_SOURCE_SELECTOR_V5_4 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction __SPLS_TMPVAR__WAITLABEL_6___Callback;


const uint SUBSCRIBE__DOLLAR____DigitalInput__ = 0;
const uint UNSUBSCRIBE__DOLLAR____DigitalInput__ = 1;
const uint INPUT__DOLLAR____AnalogSerialInput__ = 0;
const uint RX__DOLLAR____AnalogSerialInput__ = 1;
const uint INPUT_FB__DOLLAR____AnalogSerialOutput__ = 0;
const uint TX__DOLLAR____AnalogSerialOutput__ = 1;
const uint OBJECTID__DOLLAR____Parameter__ = 10;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
